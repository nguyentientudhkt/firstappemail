﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirsAppEmail.Model
{

    public class SoCanhBaoUuTienTheoSite
    {
        public string Site { get; set; }
        public string Tinh { get; set; }
        public int SoLuong { get; set; } //so luong nhay'
        public int SoLuongCongDon { get; set; }  //so luong nhay' cong don
        public int SoNgayDat { get; set; }
    }
    public class CanhBaoTongHop
    {
        public string Tinh { get; set; }
        public int SoNgayLienTuc { get; set; }
        public int TongSoNhay { get; set; }
        public int UTDB { get; set; } //
        public int UT1 { get; set; }//>7
        public int UT2 { get; set; }//3-7
        public int UT3 { get; set; }//<3
        public string DVT { get; set; }
        public int SoLuongTuan { get; set; }
        public int SoLuongThang { get; set; }
        public int ChuaXuLyTuan { get; set; }
        public int ChuaXuLyThang { get; set; }
    }
    public class XuLyOkTongHop
    {
        public string Tinh { get; set; }
        public int SoLuong { get; set; }
        public int UTDB { get; set; } //
        public int UT1 { get; set; }
        public int UT2 { get; set; }
        public int UT3 { get; set; }
        public string DVT { get; set; }
    }
    public class XuLyOkTheoSite
    {
        public string Tinh { get; set; }
        public string DVT { get; set; }
        public string Site { get; set; }
        public string Ngay { get; set; }
        public string NgayXuLy { get; set; }
        public int SoNgayKeoDai { get; set; }
    }
    public class SiteTramCanhBao
    {
        public string Site { get; set; }
        public int SoTram { get; set; }
        public int SoLanCanhBao { get; set; }
    }
    public class ChiTietToNhom
    {
        public string Site { get; set; }
        public string To { get; set; }
        public string Nhom { get; set; }
        public int TongSoTram { get; set; }
        public int SoLan1Ngay { get; set; }
        public int SoLan2Ngay { get; set; }
        public int SoLan3Ngay { get; set; }

    }
    public class Value7Days
    {
        public string Tinh { get; set; }
        public List<DateValue> DateValues { get; set; }
    }
    public class DateValue
    {
        public string SDate { get; set; }
        public int Value { get; set; }
    }
}
