﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirsAppEmail.Model
{
    public class DbManager
    {
        public SqlConnection connection = new SqlConnection();
        public SqlCommand command;
        public DbManager()
        {
            connection.ConnectionString = @"Data Source = 10.30.6.32; Initial Catalog=SYNCLOST;User ID=RIMVN;Password=rvStation123";
        }

        public DataTable SelectQuery(string strSQL)
        {
            DataTable dt = new DataTable();

            try
            {
                SqlDataAdapter da = new SqlDataAdapter(strSQL, connection);
                //if (connection.State != System.Data.ConnectionState.Open)
                //{
                //    connection.Open();
                //}
                
                da.Fill(dt);
                //connection.Close();
            }
            catch (Exception ex)
            {
                showErr(ex);
            }

            return dt;
        }
        //---------------------------------------------

        public DataTable SelectQuery(string strSQL, SqlParameter[] ThamSo)
        {
            DataTable dt = new DataTable();

            try
            {
                //if (connection.State != System.Data.ConnectionState.Open)
                //{
                //    connection.Open();
                //}
                var da = new SqlDataAdapter();
                var command = new SqlCommand(strSQL, connection);
                da.SelectCommand = command;

                // Add parameters and set values.
                for (int i = 0; i < ThamSo.Length; i++)
                {
                    command.Parameters.Add(ThamSo[i]);
                }
                da.Fill(dt);
                //connection.Close();
            }
            catch (Exception ex)
            {
                showErr(ex);
            }

            return dt;
        }
        //-----------------------------------------
        public void ExecuteQuery(string strSQL)
        {
            try
            {
                //if (connection.State != System.Data.ConnectionState.Open)
                //{
                //    connection.Open();
                //}
                var command = new SqlCommand(strSQL, connection);
                command.ExecuteNonQuery();
                //connection.Close();
            }
            catch (Exception ex)
            {
                showErr(ex);
            }
        }
        //-------------------------------------------


        //-------------------------------------------
        public void ExecuteQuery(string strSQL, SqlParameter[] ThamSo)
        {
            try
            {
                //if (connection.State != System.Data.ConnectionState.Open)
                //{
                //    connection.Open();
                //}
                var command = new SqlCommand(strSQL, connection);
                for (int i = 0; i < ThamSo.Length; i++)
                {
                    command.Parameters.Add(ThamSo[i]);
                }
                command.ExecuteNonQuery();
                //connection.Close();
            }
            catch (Exception ex)
            {
                showErr(ex);
            }
        }
        //-------------------------------------------
        public void ExecuteQuery(SqlCommand command, string strSQL)
        {
            try
            {
                //if (connection.State != System.Data.ConnectionState.Open)
                //{
                //    connection.Open();
                //}
                command.Connection = connection;
                command.CommandText = strSQL;
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                showErr(ex);
            }
        }
        //-----------------------------------------------
        public void showErr(Exception ex)
        {
            Console.Write("Error: " + ex.Message);
        }
        public void OpenConnection()
        {
            connection.Open();
        }
        public void Dispose()
        {
            connection.Close();
        }
    }
}
