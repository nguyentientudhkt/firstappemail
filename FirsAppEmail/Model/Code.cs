﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Drawing;
namespace FirsAppEmail.Model
{
    public class Code
    {

        public DateTime _tunt_dateTime = DateTime.Now;
        public SmtpClient SmtpClient()
        {
            SmtpClient smtp = new SmtpClient("10.3.12.32", 587);
            smtp.EnableSsl = false;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Credentials = new NetworkCredential("mlmt.cntt@mobifone.vn", "abcd@1234");
            smtp.Timeout = 20000;
            return smtp;
        }

        public void WriteToRichTextBox(RichTextBox r, string message)
        {
            var textLogBackup = r.Text;
            r.Text += (Environment.NewLine + _tunt_dateTime.ToString("dd/MM/yyyy hh:mm") + ": " + message).ToString();
        }
        public void WriteToRichTextBox(RichTextBox r, string message, bool isColor, Color color)
        {
            var textLogBackup = r.Text;
            int start = textLogBackup.Length;
            r.Text = (textLogBackup + Environment.NewLine + _tunt_dateTime.ToString("dd/MM/yyyy hh:mm") + ": " + message).ToString();
            int end = r.Text.Length;
            if (isColor)
            {
                r.Select(start, end - start);
                {
                    r.SelectionColor = color;
                }
                r.SelectionLength = 0; 
            }

        }
        public int _tunt_currentLineNumber()
        {
            StackFrame st = new StackFrame(1, true);
            return st.GetFileLineNumber();
        }
    }
}
