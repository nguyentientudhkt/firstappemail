﻿namespace FirsAppEmail
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.btnSendEmail = new System.Windows.Forms.Button();
            this.timerSendEmail = new System.Windows.Forms.Timer(this.components);
            this.txtDayBefore = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnTestReport = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblError = new System.Windows.Forms.Label();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.cbSendEmailDaily = new System.Windows.Forms.CheckBox();
            this.button3 = new System.Windows.Forms.Button();
            this.btnTestTUNT = new System.Windows.Forms.Button();
            this.richMessage = new System.Windows.Forms.RichTextBox();
            this.tabControl_TestEmail = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox_TestEmail = new System.Windows.Forms.GroupBox();
            this.radioButton_yn_Email_Yes = new System.Windows.Forms.RadioButton();
            this.radioButton_yn_EmailTest_No = new System.Windows.Forms.RadioButton();
            this.button4 = new System.Windows.Forms.Button();
            this.txtTimerAutoMailSynLost = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTimerAutoMailMPD = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.tabControl_TestEmail.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox_TestEmail.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSendEmail
            // 
            this.btnSendEmail.Location = new System.Drawing.Point(16, 101);
            this.btnSendEmail.Name = "btnSendEmail";
            this.btnSendEmail.Size = new System.Drawing.Size(75, 23);
            this.btnSendEmail.TabIndex = 0;
            this.btnSendEmail.Text = "Do All";
            this.btnSendEmail.UseVisualStyleBackColor = true;
            this.btnSendEmail.Click += new System.EventHandler(this.btnSendEmail_Click);
            // 
            // timerSendEmail
            // 
            this.timerSendEmail.Enabled = true;
            this.timerSendEmail.Tick += new System.EventHandler(this.timerSendEmail_Tick);
            // 
            // txtDayBefore
            // 
            this.txtDayBefore.Location = new System.Drawing.Point(97, 61);
            this.txtDayBefore.Name = "txtDayBefore";
            this.txtDayBefore.Size = new System.Drawing.Size(100, 20);
            this.txtDayBefore.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "DayBefore";
            // 
            // btnTestReport
            // 
            this.btnTestReport.Location = new System.Drawing.Point(16, 141);
            this.btnTestReport.Name = "btnTestReport";
            this.btnTestReport.Size = new System.Drawing.Size(75, 23);
            this.btnTestReport.TabIndex = 3;
            this.btnTestReport.Text = "Test Report";
            this.btnTestReport.UseVisualStyleBackColor = true;
            this.btnTestReport.Click += new System.EventHandler(this.btnTestReport_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Password";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(97, 24);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(100, 20);
            this.txtPassword.TabIndex = 4;
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Location = new System.Drawing.Point(110, 245);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 13);
            this.lblError.TabIndex = 6;
            // 
            // chart1
            // 
            chartArea2.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart1.Legends.Add(legend2);
            this.chart1.Location = new System.Drawing.Point(12, 341);
            this.chart1.Name = "chart1";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "QuangTri";
            this.chart1.Series.Add(series2);
            this.chart1.Size = new System.Drawing.Size(747, 278);
            this.chart1.TabIndex = 7;
            this.chart1.Text = "chart1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(122, 101);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Report-MPD";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(122, 141);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(92, 23);
            this.button2.TabIndex = 9;
            this.button2.Text = "Test Report KPI";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cbSendEmailDaily
            // 
            this.cbSendEmailDaily.AutoSize = true;
            this.cbSendEmailDaily.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSendEmailDaily.Location = new System.Drawing.Point(288, 140);
            this.cbSendEmailDaily.Name = "cbSendEmailDaily";
            this.cbSendEmailDaily.Size = new System.Drawing.Size(224, 24);
            this.cbSendEmailDaily.TabIndex = 10;
            this.cbSendEmailDaily.Text = "Gửi mail báo cáo hàng ngày";
            this.cbSendEmailDaily.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(619, 140);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 11;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnTestTUNT
            // 
            this.btnTestTUNT.Location = new System.Drawing.Point(619, 21);
            this.btnTestTUNT.Name = "btnTestTUNT";
            this.btnTestTUNT.Size = new System.Drawing.Size(75, 23);
            this.btnTestTUNT.TabIndex = 12;
            this.btnTestTUNT.Text = "TUNT Test ";
            this.btnTestTUNT.UseVisualStyleBackColor = true;
            this.btnTestTUNT.Click += new System.EventHandler(this.btnTestTUNT_Click);
            // 
            // richMessage
            // 
            this.richMessage.Location = new System.Drawing.Point(763, 23);
            this.richMessage.Name = "richMessage";
            this.richMessage.Size = new System.Drawing.Size(449, 148);
            this.richMessage.TabIndex = 13;
            this.richMessage.Text = "";
            // 
            // tabControl_TestEmail
            // 
            this.tabControl_TestEmail.Controls.Add(this.tabPage1);
            this.tabControl_TestEmail.Controls.Add(this.tabPage2);
            this.tabControl_TestEmail.Location = new System.Drawing.Point(763, 203);
            this.tabControl_TestEmail.Name = "tabControl_TestEmail";
            this.tabControl_TestEmail.SelectedIndex = 0;
            this.tabControl_TestEmail.Size = new System.Drawing.Size(449, 316);
            this.tabControl_TestEmail.TabIndex = 14;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtTimerAutoMailMPD);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txtTimerAutoMailSynLost);
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.groupBox_TestEmail);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(441, 290);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Test Manuall";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(441, 290);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox_TestEmail
            // 
            this.groupBox_TestEmail.Controls.Add(this.radioButton_yn_EmailTest_No);
            this.groupBox_TestEmail.Controls.Add(this.radioButton_yn_Email_Yes);
            this.groupBox_TestEmail.Location = new System.Drawing.Point(7, 20);
            this.groupBox_TestEmail.Name = "groupBox_TestEmail";
            this.groupBox_TestEmail.Size = new System.Drawing.Size(200, 114);
            this.groupBox_TestEmail.TabIndex = 0;
            this.groupBox_TestEmail.TabStop = false;
            this.groupBox_TestEmail.Text = "Test Email ";
            // 
            // radioButton_yn_Email_Yes
            // 
            this.radioButton_yn_Email_Yes.AutoSize = true;
            this.radioButton_yn_Email_Yes.Location = new System.Drawing.Point(7, 33);
            this.radioButton_yn_Email_Yes.Name = "radioButton_yn_Email_Yes";
            this.radioButton_yn_Email_Yes.Size = new System.Drawing.Size(43, 17);
            this.radioButton_yn_Email_Yes.TabIndex = 0;
            this.radioButton_yn_Email_Yes.TabStop = true;
            this.radioButton_yn_Email_Yes.Text = "Yes";
            this.radioButton_yn_Email_Yes.UseVisualStyleBackColor = true;
            // 
            // radioButton_yn_EmailTest_No
            // 
            this.radioButton_yn_EmailTest_No.AutoSize = true;
            this.radioButton_yn_EmailTest_No.Location = new System.Drawing.Point(7, 65);
            this.radioButton_yn_EmailTest_No.Name = "radioButton_yn_EmailTest_No";
            this.radioButton_yn_EmailTest_No.Size = new System.Drawing.Size(39, 17);
            this.radioButton_yn_EmailTest_No.TabIndex = 1;
            this.radioButton_yn_EmailTest_No.TabStop = true;
            this.radioButton_yn_EmailTest_No.Text = "No";
            this.radioButton_yn_EmailTest_No.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(344, 261);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 15;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // txtTimerAutoMailSynLost
            // 
            this.txtTimerAutoMailSynLost.Location = new System.Drawing.Point(7, 156);
            this.txtTimerAutoMailSynLost.Name = "txtTimerAutoMailSynLost";
            this.txtTimerAutoMailSynLost.Size = new System.Drawing.Size(200, 20);
            this.txtTimerAutoMailSynLost.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(163, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Thời gian chạy auto mail SynLost";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 188);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Thời gian chạy auto mail  MPD";
            // 
            // txtTimerAutoMailMPD
            // 
            this.txtTimerAutoMailMPD.Location = new System.Drawing.Point(7, 204);
            this.txtTimerAutoMailMPD.Name = "txtTimerAutoMailMPD";
            this.txtTimerAutoMailMPD.Size = new System.Drawing.Size(200, 20);
            this.txtTimerAutoMailMPD.TabIndex = 18;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1248, 644);
            this.Controls.Add(this.tabControl_TestEmail);
            this.Controls.Add(this.richMessage);
            this.Controls.Add(this.btnTestTUNT);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.cbSendEmailDaily);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.btnTestReport);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtDayBefore);
            this.Controls.Add(this.btnSendEmail);
            this.Name = "Form1";
            this.Text = "Sync Lost";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.tabControl_TestEmail.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox_TestEmail.ResumeLayout(false);
            this.groupBox_TestEmail.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSendEmail;
        private System.Windows.Forms.Timer timerSendEmail;
        private System.Windows.Forms.TextBox txtDayBefore;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnTestReport;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox cbSendEmailDaily;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnTestTUNT;
        private System.Windows.Forms.RichTextBox richMessage;
        private System.Windows.Forms.TabControl tabControl_TestEmail;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTimerAutoMailSynLost;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox groupBox_TestEmail;
        private System.Windows.Forms.RadioButton radioButton_yn_EmailTest_No;
        private System.Windows.Forms.RadioButton radioButton_yn_Email_Yes;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txtTimerAutoMailMPD;
        private System.Windows.Forms.Label label4;
    }
}

