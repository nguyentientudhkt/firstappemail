﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FirsAppEmail.Model;

using Oracle.ManagedDataAccess.Client;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mime;
using System.Windows.Forms.DataVisualization.Charting;

namespace FirsAppEmail
{
    public partial class Form1 : Form
    {
        public Code code = new Code();
        public Code __ = new Code();
        public int dayBefore = 1;
        public bool IsBusy = false;
        public bool IsBusyMPD = false;

        public DataTable dtTinh = new DataTable();
        public DataTable dtQuanHuyen = new DataTable();

        public List<XuLyOkTongHop> listXuLyOKTuanTongHopSyncLost = new List<XuLyOkTongHop>();
        public List<XuLyOkTongHop> listXuLyOKThangTongHopSyncLost = new List<XuLyOkTongHop>();
        public List<XuLyOkTongHop> listXuLyOKNgayTongHopSyncLost = new List<XuLyOkTongHop>();

        public List<XuLyOkTheoSite> TramPhatSinhThang = new List<XuLyOkTheoSite>();
        public List<XuLyOkTheoSite> Tram3NgayKPI = new List<XuLyOkTheoSite>();

        public List<XuLyOkTheoSite> XuLyOkNgaySyncLost = new List<XuLyOkTheoSite>();
        public List<XuLyOkTheoSite> XuLyOkUT2SyncLost = new List<XuLyOkTheoSite>();
        public List<XuLyOkTheoSite> XuLyOkUT3SyncLost = new List<XuLyOkTheoSite>();

        public List<string> listTinhSyncLost = new List<string>();
        public string rowsOfTableSyncLost = "";
        public string rowsOfTableSyncLost2 = "";
        public string rowsOfTableSyncLost3 = "";
        public string fileAttachmentPathSyncLost = "";
        public string bodySyncLost = "";
        public string bodyChartSyncLost = "";
        ChartArea areaAxisSyncLost;
        AlternateView htmlViewSyncLost;

        DateTime startDayOfWeek;
        public DbManager dbSyncLost = new DbManager();
        public OracleConnection con = new OracleConnection();

        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            dbSyncLost.OpenConnection();//Open connection sql server
            areaAxisSyncLost = chart1.ChartAreas.Add("AxisY_ChartArea1");
            // Connection oracle
            con.ConnectionString = "DATA SOURCE=10.30.6.22:1521/VHKT;PASSWORD=alarm123;USER ID=ALARM;Connection Timeout=300";
            //con.ConnectionString = "DATA SOURCE=10.30.6.22:1521/OPTIMA;PASSWORD=ktkt3123;USER ID=ktkt3;Connection Timeout=300";
            con.Open(); //open connection Oracle
            radioButton_yn_EmailTest_No.Checked = true;
            txtTimerAutoMailSynLost.Text = "08:25";
            txtTimerAutoMailMPD.Text = "07:30";
        }
        private void btnSendEmail_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPassword.Text == "19")
                {
                    dayBefore = Convert.ToInt32(txtDayBefore.Text);
                    Get_SynLost();
                    //Send email
                    SendEmail();
                    Console.Beep(5000, 500);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("err: " + ex.Message);
            }
        }
        private void AddCanhBaoUuTien(SoCanhBaoUuTienTheoSite itemUT, int loaiUT)
        {
            try
            {
                var query = "Select top 2 CASE_ALARM,TRAM, MUC_UU_TIEN, NGAY, DA_XU_LY, NGAY_XU_LY from LICHSUCANHBAO where TRAM='" + itemUT.Site + "' order by NGAY DESC";
                var dtTop2CanhBaoGanNhat = new DataTable();
                // Lay ra 2 canh bao UT 2 ngay gan nhat, cong voi ngay dang xu ly la 3 ngay
                // Kiem tra neu 3 ngay neu deu o muc UT 0, thi la tram xu ly OK

                dtTop2CanhBaoGanNhat = dbSyncLost.SelectQuery(query);
                var case_alarm = itemUT.Site + "_" + DateTime.Now.AddDays(-dayBefore).ToString("dd/MM/yyyy");
                var checkXuLyOk = true;

                string queryInsertLichSuCanhBao = "INSERT INTO LICHSUCANHBAO(CASE_ALARM,TINH,HUYEN,NGAY,TRAM,SO_LAN_NHAY,MUC_UU_TIEN,DA_XU_LY,NGAY_XU_LY) VALUES(@CASE_ALARM, @TINH, @HUYEN, @NGAY, @TRAM, @SO_LAN_NHAY, @MUC_UU_TIEN,@DA_XU_LY,@NGAY_XU_LY)";
                var command = new SqlCommand();
                var tenTinh = itemUT.Site == "" ? "" : Get_Name_Tinh_By_Id(dtTinh, itemUT.Site.Substring(0, 2));
                var tenQuanHuyen = itemUT.Site == "" ? "" : Get_Name_QuanHuyen_By_Id(dtQuanHuyen, itemUT.Site.Substring(0, 2) + itemUT.Site.Substring(2, 2));

                var daXuLy = false;
                if (dtTop2CanhBaoGanNhat.Rows.Count == 2)
                {
                    for (var i = 0; i < dtTop2CanhBaoGanNhat.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(dtTop2CanhBaoGanNhat.Rows[i][2]) != 0)
                        {
                            checkXuLyOk = false;
                            break;
                        }
                    }
                    // Tai day, neu checkXuLyOk = true, thi 2 canh bao gan nhat la o muc UT 0.
                    // Va neu canh bao dang xu ly co muc UT 0, thi tram xu ly OK, CASE_ALARM là case cũ, nếu UT != 0 va Case hien tai da xu ly thì lấy case mới
                    if (checkXuLyOk)// 2 canh bao truoc co muc UT = 0
                    {
                        if (loaiUT != 0)
                        {
                            if (Convert.ToBoolean(dtTop2CanhBaoGanNhat.Rows[0][4]))//case_alarm da xu ly
                            {
                                // loaiUT != 0 va case_alarm da xu ly-> case_alarm moi
                                case_alarm = itemUT.Site + "_" + DateTime.Now.AddDays(-dayBefore).ToString("dd/MM/yyyy");
                            }
                            else //case_alarm chua xu ly
                            {
                                // Case_alarm cu
                                case_alarm = dtTop2CanhBaoGanNhat.Rows[0][0].ToString();
                            }
                        }
                        else
                        {
                            // Case_alarm cu
                            case_alarm = dtTop2CanhBaoGanNhat.Rows[0][0].ToString();
                            daXuLy = true;
                        }
                    }
                    else
                    {
                        case_alarm = dtTop2CanhBaoGanNhat.Rows[0][0].ToString();
                    }

                    command.Parameters.Add("CASE_ALARM", SqlDbType.NVarChar).Value = case_alarm;
                    command.Parameters.Add("TINH", SqlDbType.NVarChar).Value = tenTinh;
                    command.Parameters.Add("HUYEN", SqlDbType.NVarChar).Value = tenQuanHuyen;
                    command.Parameters.Add("NGAY", SqlDbType.DateTime).Value = DateTime.Now.AddDays(-dayBefore);
                    command.Parameters.Add("TRAM", SqlDbType.VarChar).Value = itemUT.Site;
                    command.Parameters.Add("SO_LAN_NHAY", SqlDbType.Int).Value = itemUT.SoLuong;
                    command.Parameters.Add("MUC_UU_TIEN", SqlDbType.Int).Value = loaiUT;
                    command.Parameters.Add("DA_XU_LY", SqlDbType.Bit).Value = false;
                    command.Parameters.Add("NGAY_XU_LY", SqlDbType.DateTime).Value = DBNull.Value;

                    dbSyncLost.ExecuteQuery(command, queryInsertLichSuCanhBao);

                    //update xu ly ok
                    if (daXuLy)
                    {
                        //update xu ly ok cho tram dang xu ly
                        if (string.IsNullOrEmpty(dtTop2CanhBaoGanNhat.Rows[0][5].ToString())) // Chua xac dinh ngay xu ly
                        {
                            var queryUpdate = "Update LICHSUCANHBAO SET LICHSUCANHBAO.DA_XU_LY = 1, LICHSUCANHBAO.NGAY_XU_LY = '" + DateTime.Now.AddDays(-dayBefore) + "' where CASE_ALARM = '" + case_alarm + "'";
                            dbSyncLost.ExecuteQuery(queryUpdate);
                        }
                        else
                        {
                            // Da xac dinh ngay xu ly
                            var queryUpdate = "Update LICHSUCANHBAO SET LICHSUCANHBAO.DA_XU_LY = 1, LICHSUCANHBAO.NGAY_XU_LY = CAST('" + dtTop2CanhBaoGanNhat.Rows[1][5].ToString() + "' as DATETIME) where LICHSUCANHBAO.NGAY_XU_LY IS NULL and CASE_ALARM = '" + case_alarm + "'";
                            dbSyncLost.ExecuteQuery(queryUpdate);
                        }
                    }
                }
                else
                {
                    if (dtTop2CanhBaoGanNhat.Rows.Count == 1)
                    {
                        case_alarm = dtTop2CanhBaoGanNhat.Rows[0][0].ToString();
                    }
                    command.Parameters.Add("CASE_ALARM", SqlDbType.NVarChar).Value = case_alarm;
                    command.Parameters.Add("TINH", SqlDbType.NVarChar).Value = tenTinh;
                    command.Parameters.Add("HUYEN", SqlDbType.NVarChar).Value = tenQuanHuyen;
                    command.Parameters.Add("NGAY", SqlDbType.DateTime).Value = DateTime.Now.AddDays(-dayBefore);
                    command.Parameters.Add("TRAM", SqlDbType.VarChar).Value = itemUT.Site;
                    command.Parameters.Add("SO_LAN_NHAY", SqlDbType.Int).Value = itemUT.SoLuong;
                    command.Parameters.Add("MUC_UU_TIEN", SqlDbType.Int).Value = loaiUT;
                    command.Parameters.Add("DA_XU_LY", SqlDbType.Bit).Value = false;
                    command.Parameters.Add("NGAY_XU_LY", SqlDbType.DateTime).Value = DBNull.Value;

                    dbSyncLost.ExecuteQuery(command, queryInsertLichSuCanhBao);
                }
            }
            catch (Exception ex)
            {
                var a = ex;
            }
        }
        public string Get_Name_QuanHuyen_By_Id(DataTable dt, string Id)
        {
            var name = "";
            foreach (DataRow dr in dt.Rows)
            {
                if (dr[0].ToString() == Id)
                    return dr[4].ToString();
            }

            return name;
        }
        public string Get_Name_Tinh_By_Id(DataTable dt, string Id)
        {
            var name = "";
            foreach (DataRow dr in dt.Rows)
            {
                if (dr[0].ToString() == Id)
                    return dr[1].ToString();
            }

            return name;
        }
        private DateTime Get_Start_DayOfWeek()
        {
            var startDayOfWeek = DateTime.Now.AddDays(-dayBefore); // Thu 5 la ngay bat dau, thu 4 tuan sau ket thuc
            var dayOfWeek = (int)startDayOfWeek.DayOfWeek;

            if (dayOfWeek < 4)
            {
                startDayOfWeek = startDayOfWeek.AddDays(-dayOfWeek - 4 + 1);
            }
            else
            {
                startDayOfWeek = startDayOfWeek.AddDays(-dayOfWeek + 4);
            }
            return startDayOfWeek;
        }
        private void Get_Data_Xu_Ly_Ok_Ngay()
        {
            __.WriteToRichTextBox(richMessage, "Run Get_Data_Xu_Ly_Ok_Ngay");
            XuLyOkNgaySyncLost = new List<XuLyOkTheoSite>();
            XuLyOkUT2SyncLost = new List<XuLyOkTheoSite>();
            XuLyOkUT3SyncLost = new List<XuLyOkTheoSite>();

            //var queryXyLyOkNgay = "Select TINH, TRAM, FORMAT (NGAY_XU_LY, 'd', 'en-gb' ) as NGAY_XU_LY from LICHSUCANHBAO where NGAY_XU_LY >= '" + DateTime.Now.AddDays(-dayBefore).ToString("MM/dd/yyyy") + "' and NGAY_XU_LY < '" + DateTime.Now.AddDays(-dayBefore + 1).ToString("MM/dd/yyyy") + "' and DA_XU_LY = 1 and MUC_UU_TIEN > 0 GROUP BY TINH, TRAM, NGAY_XU_LY";
            var queryXyLyOkNgay = "Select TINH, TRAM, SUBSTRING(CASE_ALARM,8,10) as NGAY, " +
                                    "FORMAT (NGAY_XU_LY, 'd', 'en-gb' ) as NGAY_XU_LY, " +
                                    "DATEDIFF(d,Convert(date,SUBSTRING(CASE_ALARM,14,4)+ '-' + SUBSTRING(CASE_ALARM,11,2) +'-'+SUBSTRING(CASE_ALARM,8,2)),NGAY_XU_LY) as SONGAYKEODAI " +
                                    "from LICHSUCANHBAO where NGAY_XU_LY >= '" + DateTime.Now.AddDays(-dayBefore).ToString("MM/dd/yyyy") + "' and NGAY_XU_LY < '" + DateTime.Now.AddDays(-dayBefore + 1).ToString("MM/dd/yyyy") + "' and DA_XU_LY = 1 and MUC_UU_TIEN > 0 GROUP BY TINH, TRAM, NGAY_XU_LY,CASE_ALARM";

            var dtXuLyOkNgay = new DataTable();
            dtXuLyOkNgay = dbSyncLost.SelectQuery(queryXyLyOkNgay);

            __.WriteToRichTextBox(richMessage, "Có " + dtXuLyOkNgay.Rows.Count + " dòng dữ liệu trong bảng LICHSUCANHBAO trong ngày" + DateTime.Now.AddDays(-dayBefore).ToString("MM/dd/yyyy"));

            foreach (DataRow dr in dtXuLyOkNgay.Rows)
            {
                var item = new XuLyOkTheoSite();
                item.Site = dr["TRAM"].ToString();
                item.NgayXuLy = dr["NGAY_XU_LY"].ToString();
                item.Ngay = dr["NGAY"].ToString();
                item.SoNgayKeoDai = Convert.ToInt32(dr["SONGAYKEODAI"].ToString());
                item.Tinh = dr["TINH"].ToString();

                if (item.Tinh == "Quang Tri" || item.Tinh == "TT-Hue" || item.Tinh == "Da Nang" || item.Tinh == "Quang Nam")
                {
                    item.DVT = "ĐVTĐN";
                }
                else if (item.Tinh == "Quang Ngai" || item.Tinh == "Phu Yen" || item.Tinh == "Binh Dinh" || item.Tinh == "Khanh Hoa")
                {
                    item.DVT = "ĐVTBĐ";
                }
                else if (item.Tinh == "Gia Lai" || item.Tinh == "Kon Tum" || item.Tinh == "Dak Lak" || item.Tinh == "Dac Nong")
                {
                    item.DVT = "ĐVTĐL";
                }

                XuLyOkNgaySyncLost.Add(item);
            }
        }
        private List<XuLyOkTongHop> Get_Data_Xu_Ly_Ok_Ngay_Tong_Hop()
        {
            var queryXyLyOkNgay = "Select TINH,TRAM, CASE_ALARM, COUNT(NGAY) as SO_LUONG from LICHSUCANHBAO where NGAY_XU_LY >= '" + DateTime.Now.AddDays(-dayBefore).ToString("MM/dd/yyyy") + "' and NGAY_XU_LY < '" + DateTime.Now.AddDays(-dayBefore + 1).ToString("MM/dd/yyyy") + "' and DA_XU_LY = 1 and MUC_UU_TIEN !=0 group by TINH,CASE_ALARM, TRAM order by TINH, TRAM";
            var dtXuLyOkNgay = new DataTable();
            dtXuLyOkNgay = dbSyncLost.SelectQuery(queryXyLyOkNgay);

            var listXuLyOKNgayTongHop = new List<XuLyOkTongHop>();
            var xlOk = new XuLyOkTongHop();

            var count = 0;
            foreach (DataRow dr in dtXuLyOkNgay.Rows)
            {
                count++;
                if (xlOk.Tinh != dr["TINH"].ToString())
                {
                    if (xlOk.Tinh != null)
                    {
                        listXuLyOKNgayTongHop.Add(xlOk);
                    }

                    xlOk = new XuLyOkTongHop();
                    xlOk.Tinh = dr["TINH"].ToString();

                    xlOk.SoLuong++;
                    if (count == dtXuLyOkNgay.Rows.Count)
                    {
                        listXuLyOKNgayTongHop.Add(xlOk);
                    }
                }
                else
                {
                    xlOk.SoLuong++;

                    if (count == dtXuLyOkNgay.Rows.Count)
                    {
                        listXuLyOKNgayTongHop.Add(xlOk);
                    }
                }
            }
            return listXuLyOKNgayTongHop;
        }
        // Get Data Xu ly Ok Tuan
        private List<XuLyOkTongHop> Get_Data_Xu_Ly_Ok_Tuan()
        {
            var queryXyLyOkTuan = "Select TINH,TRAM, CASE_ALARM, COUNT(NGAY) as SO_LUONG from LICHSUCANHBAO where NGAY_XU_LY >= '" + startDayOfWeek.ToString("MM/dd/yyyy") + "' and NGAY_XU_LY < '" + startDayOfWeek.AddDays(7).ToString("MM/dd/yyyy") + "' and DA_XU_LY = 1 and MUC_UU_TIEN !=0 group by TINH,CASE_ALARM, TRAM order by TINH,TRAM";
            var dtXuLyOkTuan = new DataTable();
            dtXuLyOkTuan = dbSyncLost.SelectQuery(queryXyLyOkTuan);

            var listXuLyOKTuanTongHop = new List<XuLyOkTongHop>();
            var xlOk = new XuLyOkTongHop();

            var count = 0;
            foreach (DataRow dr in dtXuLyOkTuan.Rows)
            {
                count++;
                if (xlOk.Tinh != dr["TINH"].ToString())
                {
                    if (xlOk.Tinh != null)
                    {
                        listXuLyOKTuanTongHop.Add(xlOk);
                    }

                    xlOk = new XuLyOkTongHop();
                    xlOk.Tinh = dr["TINH"].ToString();

                    xlOk.SoLuong++;

                    if (count == dtXuLyOkTuan.Rows.Count)
                    {
                        listXuLyOKTuanTongHop.Add(xlOk);
                    }
                }
                else
                {
                    xlOk.SoLuong++;
                    if (count == dtXuLyOkTuan.Rows.Count)
                    {
                        listXuLyOKTuanTongHop.Add(xlOk);
                    }
                }
            }
            return listXuLyOKTuanTongHop;
        }

        private List<XuLyOkTongHop> Get_Data_Xu_Ly_Ok_Thang()
        {
            var date = DateTime.Now.AddDays(-dayBefore);
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var queryXyLyOkThang = "Select TINH,TRAM, CASE_ALARM, COUNT(NGAY) as SO_LUONG from LICHSUCANHBAO where NGAY_XU_LY >= '" + firstDayOfMonth.ToString("MM/dd/yyyy") + "' and NGAY_XU_LY < '" + firstDayOfMonth.AddMonths(1).ToString("MM/dd/yyyy") + "' and DA_XU_LY = 1 and MUC_UU_TIEN >0 group by TINH,CASE_ALARM, TRAM order by TINH, TRAM";
            var dtXuLyOkThang = new DataTable();
            dtXuLyOkThang = dbSyncLost.SelectQuery(queryXyLyOkThang);

            var listXuLyOKThangTongHop = new List<XuLyOkTongHop>();
            var xlOk = new XuLyOkTongHop();
            var count = 0;
            foreach (DataRow dr in dtXuLyOkThang.Rows)
            {
                count++;
                if (xlOk.Tinh != dr["TINH"].ToString())
                {
                    if (xlOk.Tinh != null)
                    {
                        listXuLyOKThangTongHop.Add(xlOk);
                    }

                    xlOk = new XuLyOkTongHop();
                    xlOk.Tinh = dr["TINH"].ToString();

                    xlOk.SoLuong++;

                    if (count == dtXuLyOkThang.Rows.Count)
                    {
                        listXuLyOKThangTongHop.Add(xlOk);
                    }
                }
                else
                {
                    xlOk.SoLuong++;

                    if (count == dtXuLyOkThang.Rows.Count)
                    {
                        listXuLyOKThangTongHop.Add(xlOk);
                    }
                }
            }
            return listXuLyOKThangTongHop;
        }

        /// <summary>
        /// Hàm Get_SynLost 
        /// B1: GET DATA CANH BAO NGAY TU ORACLE
        /// B2: GET DATA TINH THANH TU ORACLE 
        /// B3: GET DATA QUAN HUYEN TU ORACLE 
        /// B4: XU LY SO LIEU, TINH CANH BAO NGAY VA TINH TOAN XU LY OK (Insert dữ liệu vào bảng LICHSUCANHBAO thông qua hàm AddCanhBaoUuTien với từng mức độ ưu tiên khác nhau) 
        /// Add dữ liệu LICHSUCANHBAO của ngày hôm qua
        /// B5: GET DATA CẢNH BÁO NGÀY TỔNG HỢP thông qua list listCanhBaoNgayTongHop
        /// B6: GET DATA CẢNH BÁO TUẦN TỔNG HỢP thông qua list listCanhBaoTuanTongHop = listCanhBaoNgayTongHop;
        /// B7: GET DATA CANH BAO THANG TONG HOP thông qua list listCanhBaoThangTongHop = listCanhBaoNgayTongHop;
        /// B: Get_Data_Xu_Ly_Ok_Ngay() -- Phuc vu cho bang XU LY OK NGAY
        /// </summary>
        private void Get_SynLost()
        {
            __.WriteToRichTextBox(richMessage, "Start Get_SynLost()");
            IsBusy = true;
            startDayOfWeek = Get_Start_DayOfWeek();
            try
            {
                #region INIT VARIABLES
                var UT1 = new List<SoCanhBaoUuTienTheoSite>();
                var UT2 = new List<SoCanhBaoUuTienTheoSite>();
                var UT3 = new List<SoCanhBaoUuTienTheoSite>();

                var comd = new OracleCommand();
                comd.Connection = con;

                var comdLog = new OracleCommand();
                comdLog.Connection = con;
                #endregion

                #region GET DATA CANH BAO NGAY TU ORACLE
                // du lieu so lan canh bao ngay hien tai group by SITE_ID
                comd.CommandText = "select SITE as SITE_ID, Count(SITE) as SO_LAN from R_alarm_log_active" +
                " where SDATE >=SYSDATE-" + (dayBefore) +
                " and SDATE <SYSDATE-" + (dayBefore - 1) +
                " and ALARM_NAME like '%lost%'" +
                " and SITE not like 'DKM%'" +
                " and SITE not like 'R%'" +
                " and SITE is not null" +
                " and VENDOR like 'NOKIA%'" +
                " group by SITE";

                comdLog.CommandText = "select SITE as SITE_ID, Count(SITE) as SO_LAN from R_alarm_log" +
                " where SDATE >=SYSDATE-" + (dayBefore) +
                " and SDATE <SYSDATE-" + (dayBefore - 1) +
                " and ALARM_NAME like '%lost%'" +
                " and SITE not like 'DKM%'" +
                " and SITE not like 'R%'" +
                " and SITE is not null" +
                " and VENDOR like 'NOKIA%'" +
                " group by SITE";

                comd.CommandType = CommandType.Text;
                var da = new OracleDataAdapter(comd);
                var dt = new DataTable();// datatable du lieu tu oracle bang active
                da.Fill(dt); //Data 1 ngay lay tu oracle

                comdLog.CommandType = CommandType.Text;
                var daLog = new OracleDataAdapter(comdLog);
                var dtLog = new DataTable();// datatable du lieu tu oracle bang log
                daLog.Fill(dtLog); //Data 1 ngay lay tu oracle
                var c1 = dtLog.Rows.Count;
                var c2 = dt.Rows.Count;


                var listSiteAdd = new List<DataRow>();

                for (var i = 0; i < dtLog.Rows.Count; i++)
                {
                    var isExisted = false;
                    for (var j = 0; j < dt.Rows.Count; j++)
                    {
                        if (dtLog.Rows[i]["SITE_ID"].ToString() == dt.Rows[j]["SITE_ID"].ToString())
                        {
                            isExisted = true;
                            dt.Rows[j]["SO_LAN"] = Convert.ToInt32(dt.Rows[j]["SO_LAN"]) + Convert.ToInt32(dtLog.Rows[i]["SO_LAN"]);
                            break;
                        }
                    }
                    if (!isExisted)
                    {
                        DataRow dr = dt.NewRow();
                        dr["SITE_ID"] = dtLog.Rows[i]["SITE_ID"].ToString();
                        dr["SO_LAN"] = Convert.ToInt32(dtLog.Rows[i]["SO_LAN"]);
                        listSiteAdd.Add(dr);
                    }
                }
                for (var i = 0; i < listSiteAdd.Count; i++)
                {
                    dt.Rows.Add(listSiteAdd[i]);
                }
                #endregion

                #region GET DATA TINH THANH
                comd.CommandText = "select TINHID,TENTINH from AL_QUANHUYEN group by TINHID,TENTINH";
                da = new OracleDataAdapter(comd);
                da.Fill(dtTinh);// Data Tinh
                #endregion

                #region GET DATA QUAN HUYEN
                comd.CommandText = "Select * from AL_QUANHUYEN";
                da = new OracleDataAdapter(comd);
                da.Fill(dtQuanHuyen);// Data Huyen
                #endregion

                #region XU LY SO LIEU, TINH CANH BAO NGAY VA TINH TOAN XU LY OK

                var listNewSite = new List<string>(); // list site xuat hien canh bao ngay truoc

                // datatable dt: site va so lan nhay cua site do trong ngay truoc.

                #region THEM CANH BAO VAO DB SQLSERVER
                foreach (DataRow dr in dt.Rows)
                {
                    var itemUT = new SoCanhBaoUuTienTheoSite();
                    itemUT.Site = dr["SITE_ID"].ToString();

                    listNewSite.Add(itemUT.Site);

                    itemUT.SoLuong = Convert.ToInt32(dr["SO_LAN"]);

                    if (!(itemUT.Site[0] == 'B' && char.IsLetter(itemUT.Site[4])) && !(itemUT.Site[0] == 'N' && itemUT.Site[1] == 'A'))
                    {
                        // Neu so luong nhay' trong 1 ngay > 20 => Uu Tien 1

                        if (itemUT.SoLuong > 20)
                        {
                            AddCanhBaoUuTien(itemUT, 1); // Them du lieu vao data SQL server 
                        }
                        // Neu so nhay' 1 ngay >= 10 => Uu Tien 2
                        else if (itemUT.SoLuong >= 10)
                        {
                            AddCanhBaoUuTien(itemUT, 2); // Them du lieu vao data SQL server 

                        }
                        else
                        {
                            AddCanhBaoUuTien(itemUT, 0); // Them du lieu vao data SQL server 
                        }
                    }
                }
                #endregion

                //Query lay tat ca Tram co case_alarm open
                var querySiteOpen = "select CASE_ALARM, TRAM from LICHSUCANHBAO where DA_XU_LY = 0 group by CASE_ALARM, TRAM";

                var dtSiteOpen = new DataTable();
                dtSiteOpen = dbSyncLost.SelectQuery(querySiteOpen);
                var listSiteOpen = new List<string>(); //list site canh bao open ton tai 

                foreach (DataRow dr in dtSiteOpen.Rows)
                {
                    listSiteOpen.Add(dr["TRAM"].ToString());
                }

                // Site ma Case_alarm van open
                // nhung ngay hien tai khong co canh bao thi tao canh bao voi so lan nhay = 0, muc uu tien 0
                for (var i = 0; i < listSiteOpen.Count; i++)
                {
                    if (!listNewSite.Any(s => s == listSiteOpen[i]))
                    {
                        var itemUT = new SoCanhBaoUuTienTheoSite();
                        itemUT.Site = listSiteOpen[i];
                        itemUT.SoLuong = 0;
                        AddCanhBaoUuTien(itemUT, 0);
                    }
                }

                #endregion

                #region GET DATA CANH BAO NGAY TONG HOP
                //Data ngay Tong hop
                var query = "Select TINH,TRAM, CASE_ALARM, COUNT(NGAY) as SO_NGAY, SUM(SO_LAN_NHAY) as TONG_SO_NHAY" +
                          " from LICHSUCANHBAO where DA_XU_LY = 0 and MUC_UU_TIEN > 0 and NGAY < '" + DateTime.Now.AddDays(-dayBefore + 1).ToString("MM/dd/yyyy") + "' " +
                          "GROUP BY TINH,CASE_ALARM, TRAM order by TINH";
                var dtCanhBaoNgay = new DataTable();
                dtCanhBaoNgay = dbSyncLost.SelectQuery(query);
                var listCanhBaoNgayTongHop = new List<CanhBaoTongHop>();
                var cb = new CanhBaoTongHop();
                var countRow = 0;
                foreach (DataRow dr in dtCanhBaoNgay.Rows)
                {
                    countRow++;
                    var itemUT = new SoCanhBaoUuTienTheoSite();
                    itemUT.Site = dr["TRAM"].ToString();
                    itemUT.Tinh = dr["TINH"].ToString();
                    itemUT.SoLuongCongDon = Convert.ToInt32(dr["TONG_SO_NHAY"]);
                    itemUT.SoNgayDat = Convert.ToInt32(dr["SO_NGAY"]);

                    if (cb.Tinh != dr["TINH"].ToString())
                    {
                        if (!string.IsNullOrEmpty(cb.Tinh))
                        {
                            listCanhBaoNgayTongHop.Add(cb);
                            cb = new CanhBaoTongHop();
                        }
                        cb.TongSoNhay += itemUT.SoLuongCongDon;
                        cb.Tinh = dr["TINH"].ToString();
                        cb.DVT = "KXD";

                        if (cb.Tinh == "Quang Tri" || cb.Tinh == "TT-Hue" || cb.Tinh == "Da Nang" || cb.Tinh == "Quang Nam")
                        {
                            cb.DVT = "ĐVTĐN";
                        }
                        else if (cb.Tinh == "Quang Ngai" || cb.Tinh == "Phu Yen" || cb.Tinh == "Binh Dinh" || cb.Tinh == "Khanh Hoa")
                        {
                            cb.DVT = "ĐVTBĐ";
                        }
                        else if (cb.Tinh == "Gia Lai" || cb.Tinh == "Kon Tum" || cb.Tinh == "Dak Lak" || cb.Tinh == "Dac Nong")
                        {
                            cb.DVT = "ĐVTĐL";
                        }
                        var soLuongLienTuc = Convert.ToInt32(dr["SO_NGAY"]);

                        if (soLuongLienTuc > 5)
                        {
                            cb.UT1++; //Nhom 1
                            UT1.Add(itemUT);
                        }
                        else if (soLuongLienTuc > 3)
                        {
                            cb.UT2++; //Nhom 2
                            UT2.Add(itemUT);
                        }
                        else
                        {
                            cb.UT3++; //Nhom 3
                            UT3.Add(itemUT);
                        }

                        if (countRow == dtCanhBaoNgay.Rows.Count)
                        {
                            listCanhBaoNgayTongHop.Add(cb);
                        }
                    }
                    else
                    {
                        cb.TongSoNhay += itemUT.SoLuongCongDon;
                        var soLuongLienTuc = Convert.ToInt32(dr["SO_NGAY"]);

                        if (soLuongLienTuc > 5)
                        {
                            cb.UT1++; //Nhom 1
                            UT1.Add(itemUT);
                        }
                        else if (soLuongLienTuc > 3)
                        {
                            cb.UT2++; //Nhom 2
                            UT2.Add(itemUT);
                        }
                        else
                        {
                            cb.UT3++; //Nhom 3
                            UT3.Add(itemUT);
                        }
                        if (countRow == dtCanhBaoNgay.Rows.Count)
                        {
                            listCanhBaoNgayTongHop.Add(cb);
                        }
                    }


                    //var mucUT = Convert.ToInt32(dr["MUC_UU_TIEN"]);

                    //if (!(itemUT.Site[0] == 'B' && char.IsLetter(itemUT.Site[4])) && (itemUT.Site[0] != 'N' && itemUT.Site[1] != 'A'))
                    //{
                    //    // Neu so luong nhay' trong 1 ngay > 20 => Uu Tien 1

                    //    if (mucUT == 1)
                    //    {
                    //        UT1.Add(itemUT);
                    //        var queryCheck = "Select CASE_ALARM, TRAM,TINH, NGAY, SO_LAN_NHAY, MUC_UU_TIEN, DA_XU_LY from LICHSUCANHBAO where DA_XU_LY = 0 and MUC_UU_TIEN = 1 and TRAM = '" + itemUT.Site + "' and NGAY < '" + DateTime.Now.AddDays(-dayBefore + 1).ToString("MM/dd/yyyy") + "' order by CASE_ALARM";
                    //        var dtCheck = new DataTable();
                    //        dtCheck = db.SelectQuery(queryCheck);
                    //        itemUT.SoNgayDat = dtCheck.Rows.Count;
                    //    }
                    //    // Neu so nhay' 1 ngay > 10 => Uu Tien 2
                    //    else if (mucUT == 2)
                    //    {
                    //        UT2.Add(itemUT);
                    //        var queryCheck = "Select CASE_ALARM, TRAM,TINH, NGAY, SO_LAN_NHAY, MUC_UU_TIEN, DA_XU_LY from LICHSUCANHBAO where DA_XU_LY = 0 and MUC_UU_TIEN = 2 and TRAM = '" + itemUT.Site + "' and NGAY < '" + DateTime.Now.AddDays(-dayBefore + 1).ToString("MM/dd/yyyy") + "' order by CASE_ALARM";
                    //        var dtCheck = new DataTable();
                    //        dtCheck = db.SelectQuery(queryCheck);
                    //        itemUT.SoNgayDat = dtCheck.Rows.Count;
                    //    }
                    //}
                }

                var queryTimNhayTheoNgay = "Select TRAM, SO_LAN_NHAY from LICHSUCANHBAO where DA_XU_LY = 0 and NGAY >= '" + DateTime.Now.AddDays(-dayBefore).ToString("MM/dd/yyyy") + "' and NGAY < '" + DateTime.Now.AddDays(-dayBefore + 1).ToString("MM/dd/yyyy") + "'order by TRAM";
                var dtTramNhay = new DataTable();
                dtTramNhay = dbSyncLost.SelectQuery(queryTimNhayTheoNgay);

                foreach (DataRow dr in dtTramNhay.Rows)
                {
                    var soNhayNgay = Convert.ToInt32(dr["SO_LAN_NHAY"].ToString());
                    var site = dr["TRAM"].ToString();
                    for (var i = 0; i < UT1.Count; i++)
                    {
                        if (UT1[i].Site == site)
                        {
                            UT1[i].SoLuong = soNhayNgay;
                        }
                    }
                    for (var i = 0; i < UT2.Count; i++)
                    {
                        if (UT2[i].Site == site)
                        {
                            UT2[i].SoLuong = soNhayNgay;
                        }
                    }
                    for (var i = 0; i < UT3.Count; i++)
                    {
                        if (UT3[i].Site == site)
                        {
                            UT3[i].SoLuong = soNhayNgay;
                        }
                    }
                }
                #endregion

                #region GET DATA CANH BAO TUAN TONG HOP
                var listCanhBaoTuanTongHop = new List<CanhBaoTongHop>();
                listCanhBaoTuanTongHop = listCanhBaoNgayTongHop;

                //Data ngay Tong hop
                //var queryCanhBaoTuan = "Select TINH,TRAM, CASE_ALARM, MUC_UU_TIEN,  COUNT(NGAY) as SO_LUONG from LICHSUCANHBAO where DA_XU_LY = 0 and NGAY < '" + DateTime.Now.AddDays(-dayBefore + 1).ToString("MM/dd/yyyy") + "' GROUP BY MUC_UU_TIEN, TINH,CASE_ALARM, TRAM order by TINH, MUC_UU_TIEN";
                //var dtCanhBaoTuan = new DataTable();
                //dtCanhBaoTuan = db.SelectQuery(queryCanhBaoTuan);

                //var cbTuan = new CanhBaoTongHop();
                //var countRowTuan = 0;
                //foreach (DataRow dr in dtCanhBaoTuan.Rows)
                //{
                //    countRowTuan++;
                //    if (cbTuan.Tinh != dr["TINH"].ToString())
                //    {
                //        if (cbTuan.Tinh != null)
                //        {
                //            listCanhBaoTuanTongHop.Add(cbTuan);
                //            cbTuan = new CanhBaoTongHop();
                //        }

                //        cbTuan.Tinh = dr["TINH"].ToString();
                //        var mucUuuTien = Convert.ToInt32(dr["MUC_UU_TIEN"]);
                //        var soLuong = Convert.ToInt32(dr["SO_LUONG"]);
                //        if (mucUuuTien == 1)
                //        {
                //            cbTuan.UT1 += soLuong;
                //        }
                //        else if (mucUuuTien == 2)
                //        {
                //            cbTuan.UT2 += soLuong;
                //        }

                //        if (countRowTuan == dtCanhBaoTuan.Rows.Count)
                //        {
                //            listCanhBaoTuanTongHop.Add(cbTuan);
                //        }
                //    }
                //    else
                //    {
                //        var mucUuuTien = Convert.ToInt32(dr["MUC_UU_TIEN"]);
                //        var soLuong = Convert.ToInt32(dr["SO_LUONG"]);
                //        if (mucUuuTien == 1)
                //        {
                //            cbTuan.UT1 += soLuong;
                //        }
                //        else if (mucUuuTien == 2)
                //        {
                //            cbTuan.UT2 += soLuong;
                //        }

                //        if (countRowTuan == dtCanhBaoTuan.Rows.Count)
                //        {
                //            listCanhBaoTuanTongHop.Add(cbTuan);
                //        }
                //    }
                //}

                #endregion

                #region GET DATA CANH BAO THANG TONG HOP

                var listCanhBaoThangTongHop = new List<CanhBaoTongHop>();
                listCanhBaoThangTongHop = listCanhBaoNgayTongHop;

                //Data Thang Tong hop
                //var date = DateTime.Now.AddDays(-dayBefore);
                //var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);

                //var queryCanhBaoThang = "Select TINH,TRAM, CASE_ALARM, MUC_UU_TIEN,  COUNT(NGAY) as SO_LUONG from LICHSUCANHBAO where DA_XU_LY = 0 and NGAY < '" + DateTime.Now.AddDays(-dayBefore + 1).ToString("MM/dd/yyyy") + "' GROUP BY MUC_UU_TIEN, TINH,CASE_ALARM, TRAM order by TINH, MUC_UU_TIEN";
                //var dtCanhBaoThang = new DataTable();
                //dtCanhBaoThang = db.SelectQuery(queryCanhBaoThang);

                //var cbThang = new CanhBaoTongHop();
                //var countRowThang = 0;
                //foreach (DataRow dr in dtCanhBaoThang.Rows)
                //{
                //    countRowThang++;
                //    if (cbThang.Tinh != dr["TINH"].ToString())
                //    {
                //        if (cbThang.Tinh != null)
                //        {
                //            listCanhBaoThangTongHop.Add(cbThang);
                //            cbThang = new CanhBaoTongHop();
                //        }

                //        cbThang.Tinh = dr["TINH"].ToString();
                //        var mucUuuTien = Convert.ToInt32(dr["MUC_UU_TIEN"]);
                //        var soLuong = Convert.ToInt32(dr["SO_LUONG"]);
                //        if (mucUuuTien == 1)
                //        {
                //            cbThang.UT1 += soLuong;
                //        }
                //        else if (mucUuuTien == 2)
                //        {
                //            cbThang.UT2 += soLuong;
                //        }

                //        if (countRowThang == dtCanhBaoThang.Rows.Count)
                //        {
                //            listCanhBaoThangTongHop.Add(cbThang);
                //        }
                //    }
                //    else
                //    {
                //        var mucUuuTien = Convert.ToInt32(dr["MUC_UU_TIEN"]);
                //        var soLuong = Convert.ToInt32(dr["SO_LUONG"]);
                //        if (mucUuuTien == 1)
                //        {
                //            cbThang.UT1 += soLuong;
                //        }
                //        else if (mucUuuTien == 2)
                //        {
                //            cbThang.UT2 += soLuong;
                //        }

                //        if (countRowThang == dtCanhBaoThang.Rows.Count)
                //        {
                //            listCanhBaoThangTongHop.Add(cbThang);
                //        }
                //    }
                //}

                #endregion

                #region GET DATA NGAY XU LY OK - SELECT FROM LICHSUCANHBAO
                Get_Data_Xu_Ly_Ok_Ngay();//Phuc vu cho bang XU LY OK NGAY

                listXuLyOKNgayTongHopSyncLost = Get_Data_Xu_Ly_Ok_Ngay_Tong_Hop();
                #endregion

                #region GET DATA TUAN XU LY OK

                listXuLyOKTuanTongHopSyncLost = Get_Data_Xu_Ly_Ok_Tuan();

                #endregion

                #region GET DATA THANG XU LY OK

                listXuLyOKThangTongHopSyncLost = Get_Data_Xu_Ly_Ok_Thang();

                #endregion

                #region TAO CONTENT EMAIL TABLE TONG HOP

                var tongHopDai = new CanhBaoTongHop();

                var listTinhFull = new List<string>();
                listTinhFull.Add("Quang Ngai");
                listTinhFull.Add("Khanh Hoa");
                listTinhFull.Add("Da Nang");
                listTinhFull.Add("Dac Nong");
                listTinhFull.Add("Quang Nam");
                listTinhFull.Add("TT-Hue");
                listTinhFull.Add("Binh Dinh");
                listTinhFull.Add("Kon Tum");
                listTinhFull.Add("Phu Yen");
                listTinhFull.Add("Dak Lak");
                listTinhFull.Add("Quang Tri");
                listTinhFull.Add("Gia Lai");

                listTinhSyncLost = listCanhBaoNgayTongHop.Select(_ => _.Tinh).ToList();

                for (var j = 0; j < listTinhFull.Count; j++)
                {
                    if (!listTinhSyncLost.Any(x => x == listTinhFull[j]))
                    {
                        cb = new CanhBaoTongHop();
                        cb.Tinh = listTinhFull[j];
                        if (cb.Tinh == "Quang Tri" || cb.Tinh == "TT-Hue" || cb.Tinh == "Da Nang" || cb.Tinh == "Quang Nam")
                        {
                            cb.DVT = "ĐVTĐN";
                        }
                        else if (cb.Tinh == "Quang Ngai" || cb.Tinh == "Phu Yen" || cb.Tinh == "Binh Dinh" || cb.Tinh == "Khanh Hoa")
                        {
                            cb.DVT = "ĐVTBĐ";
                        }
                        else if (cb.Tinh == "Gia Lai" || cb.Tinh == "Kon Tum" || cb.Tinh == "Dak Lak" || cb.Tinh == "Dac Nong")
                        {
                            cb.DVT = "ĐVTĐL";
                        }

                        listCanhBaoNgayTongHop.Add(cb);
                    }
                }
                var listCanhBaoNgayTongHopSort = listCanhBaoNgayTongHop.OrderBy(_ => _.DVT).ToList();
                for (var j = 0; j < listCanhBaoNgayTongHopSort.Count(); j++)
                {

                    var xuLyOkNgayByTinh = listXuLyOKNgayTongHopSyncLost.FirstOrDefault(_ => _.Tinh == listCanhBaoNgayTongHopSort[j].Tinh);
                    if (xuLyOkNgayByTinh == null)
                    {
                        xuLyOkNgayByTinh = new XuLyOkTongHop();
                    }

                    var xuLyOkTuanByTinh = listXuLyOKTuanTongHopSyncLost.FirstOrDefault(_ => _.Tinh == listCanhBaoNgayTongHopSort[j].Tinh);
                    if (xuLyOkTuanByTinh == null)
                    {
                        xuLyOkTuanByTinh = new XuLyOkTongHop();
                    }
                    var xuLyOkThangByTinh = listXuLyOKThangTongHopSyncLost.FirstOrDefault(_ => _.Tinh == listCanhBaoNgayTongHopSort[j].Tinh);
                    if (xuLyOkThangByTinh == null)
                    {
                        xuLyOkThangByTinh = new XuLyOkTongHop();
                    }

                    var canhBaoTuanByTinh = listCanhBaoTuanTongHop.FirstOrDefault(_ => _.Tinh == listCanhBaoNgayTongHopSort[j].Tinh);
                    if (canhBaoTuanByTinh == null)
                    {
                        canhBaoTuanByTinh = new CanhBaoTongHop();
                    }

                    var canhBaoThangByTinh = listCanhBaoThangTongHop.FirstOrDefault(_ => _.Tinh == listCanhBaoNgayTongHopSort[j].Tinh);
                    if (canhBaoThangByTinh == null)
                    {
                        canhBaoThangByTinh = new CanhBaoTongHop();
                    }

                    if (tongHopDai.DVT != listCanhBaoNgayTongHopSort[j].DVT)
                    {
                        if (tongHopDai.DVT != null)
                        {
                            rowsOfTableSyncLost2 += "<tr>" +

                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #98b2e4; white-space: nowrap;'>" + tongHopDai.DVT + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #bdbc80; white-space: nowrap; font-style: italic'>Tổng</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7' >" + (tongHopDai.UT1) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7' >" + (tongHopDai.UT2) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7' >" + (tongHopDai.UT3) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7' >" + (tongHopDai.UT1 + tongHopDai.UT2 + tongHopDai.UT3) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7'>" + (tongHopDai.SoLuongTuan) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7'>" + (tongHopDai.ChuaXuLyTuan) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7'>" + (tongHopDai.SoLuongTuan == 0 ? 0 : 100 * (tongHopDai.ChuaXuLyTuan) / (tongHopDai.SoLuongTuan)) + "%</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7'>" + (tongHopDai.SoLuongThang) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7'>" + (tongHopDai.ChuaXuLyThang) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7'>" + (tongHopDai.SoLuongThang == 0 ? 0 : 100 * (tongHopDai.ChuaXuLyThang) / (tongHopDai.SoLuongThang)) + "%</td>" +

                                         "</tr>";
                            tongHopDai = new CanhBaoTongHop();
                        }
                        tongHopDai.DVT = listCanhBaoNgayTongHopSort[j].DVT;
                        tongHopDai.Tinh = " ";
                        tongHopDai.UT1 += listCanhBaoNgayTongHopSort[j].UT1;
                        tongHopDai.UT2 += listCanhBaoNgayTongHopSort[j].UT2;
                        tongHopDai.UT3 += listCanhBaoNgayTongHopSort[j].UT3;
                        tongHopDai.SoLuongTuan += canhBaoTuanByTinh.UT1 + canhBaoTuanByTinh.UT2 + canhBaoTuanByTinh.UT3 + xuLyOkTuanByTinh.SoLuong;
                        tongHopDai.SoLuongThang += canhBaoThangByTinh.UT1 + canhBaoThangByTinh.UT2 + canhBaoThangByTinh.UT3 + xuLyOkThangByTinh.SoLuong;
                        tongHopDai.ChuaXuLyTuan += canhBaoTuanByTinh.UT1 + canhBaoTuanByTinh.UT2 + canhBaoTuanByTinh.UT3;
                        tongHopDai.ChuaXuLyThang += canhBaoThangByTinh.UT1 + canhBaoThangByTinh.UT2 + canhBaoThangByTinh.UT3;
                    }
                    else
                    {
                        tongHopDai.DVT = listCanhBaoNgayTongHopSort[j].DVT;
                        tongHopDai.Tinh = "";
                        tongHopDai.UT1 += listCanhBaoNgayTongHopSort[j].UT1;
                        tongHopDai.UT2 += listCanhBaoNgayTongHopSort[j].UT2;
                        tongHopDai.UT3 += listCanhBaoNgayTongHopSort[j].UT3;
                        tongHopDai.SoLuongTuan += canhBaoTuanByTinh.UT1 + canhBaoTuanByTinh.UT2 + canhBaoTuanByTinh.UT3 + xuLyOkTuanByTinh.SoLuong;
                        tongHopDai.SoLuongThang += canhBaoThangByTinh.UT1 + canhBaoThangByTinh.UT2 + canhBaoThangByTinh.UT3 + xuLyOkThangByTinh.SoLuong;
                        tongHopDai.ChuaXuLyTuan += canhBaoTuanByTinh.UT1 + canhBaoTuanByTinh.UT2 + canhBaoTuanByTinh.UT3;
                        tongHopDai.ChuaXuLyThang += canhBaoThangByTinh.UT1 + canhBaoThangByTinh.UT2 + canhBaoThangByTinh.UT3;
                    }
                    rowsOfTableSyncLost2 += "<tr>" +

                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #98b2e4; white-space: nowrap;'>" + listCanhBaoNgayTongHopSort[j].DVT + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #bdbc80; white-space: nowrap;'>" + listCanhBaoNgayTongHopSort[j].Tinh + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #FFF' >" + (listCanhBaoNgayTongHopSort[j].UT1) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #FFF' >" + (listCanhBaoNgayTongHopSort[j].UT2) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #FFF' >" + (listCanhBaoNgayTongHopSort[j].UT3) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #FFF' >" + (listCanhBaoNgayTongHopSort[j].UT1 + listCanhBaoNgayTongHopSort[j].UT2 + listCanhBaoNgayTongHopSort[j].UT3) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;'>" + (canhBaoTuanByTinh.UT1 + canhBaoTuanByTinh.UT2 + canhBaoTuanByTinh.UT3 + xuLyOkTuanByTinh.SoLuong) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;'>" + (canhBaoTuanByTinh.UT1 + canhBaoTuanByTinh.UT2 + canhBaoTuanByTinh.UT3) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;'>" + (canhBaoTuanByTinh.UT1 + canhBaoTuanByTinh.UT2 + canhBaoTuanByTinh.UT3 + xuLyOkTuanByTinh.SoLuong == 0 ? 0 : 100 * (canhBaoTuanByTinh.UT1 + canhBaoTuanByTinh.UT2 + canhBaoTuanByTinh.UT3) / (canhBaoTuanByTinh.UT1 + canhBaoTuanByTinh.UT2 + canhBaoTuanByTinh.UT3 + xuLyOkTuanByTinh.SoLuong)) + "%</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;'>" + (canhBaoThangByTinh.UT1 + canhBaoThangByTinh.UT2 + canhBaoThangByTinh.UT3 + xuLyOkThangByTinh.SoLuong) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;'>" + (canhBaoThangByTinh.UT1 + canhBaoThangByTinh.UT2 + canhBaoThangByTinh.UT3) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;'>" + (canhBaoThangByTinh.UT1 + canhBaoThangByTinh.UT2 + canhBaoThangByTinh.UT3 + xuLyOkThangByTinh.SoLuong == 0 ? 0 : 100 * (canhBaoThangByTinh.UT1 + canhBaoThangByTinh.UT2 + canhBaoThangByTinh.UT3) / (canhBaoThangByTinh.UT1 + canhBaoThangByTinh.UT2 + canhBaoThangByTinh.UT3 + xuLyOkThangByTinh.SoLuong)) + "%</td>" +

                                         "</tr>";

                    var tongCaseOpen = (listCanhBaoNgayTongHopSort[j].UT1 + listCanhBaoNgayTongHopSort[j].UT2 + listCanhBaoNgayTongHopSort[j].UT3);
                    string queryInsertSoLieuCaseOpen = "INSERT INTO SOLIEUCASEALARM(TINH,NGAY,SO_CASE_OPEN) VALUES(@TINH, @NGAY, @SO_CASE_OPEN)";
                    var command = new SqlCommand();

                    command.Parameters.Add("TINH", SqlDbType.NVarChar).Value = listCanhBaoNgayTongHopSort[j].Tinh;
                    command.Parameters.Add("NGAY", SqlDbType.DateTime).Value = DateTime.Now.AddDays(-dayBefore);
                    command.Parameters.Add("SO_CASE_OPEN", SqlDbType.Int).Value = tongCaseOpen;

                    var queryCheckExist = "Select * from SOLIEUCASEALARM where TINH='" + listCanhBaoNgayTongHopSort[j].Tinh + "' and NGAY='" + DateTime.Now.AddDays(-dayBefore).ToString("MM-dd-yyyy") + "'";

                    var dtCheck = dbSyncLost.SelectQuery(queryCheckExist);

                    if (dtCheck.Rows.Count == 0)
                    {
                        dbSyncLost.ExecuteQuery(command, queryInsertSoLieuCaseOpen);
                    }

                    if (j == listCanhBaoNgayTongHopSort.Count() - 1)
                    {
                        rowsOfTableSyncLost2 += "<tr>" +

                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #98b2e4; white-space: nowrap;'>" + tongHopDai.DVT + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #bdbc80; white-space: nowrap; font-style: italic'>Tổng</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7' >" + (tongHopDai.UT1) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7' >" + (tongHopDai.UT2) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7' >" + (tongHopDai.UT3) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7' >" + (tongHopDai.UT1 + tongHopDai.UT2 + tongHopDai.UT3) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7'>" + (tongHopDai.SoLuongTuan) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7'>" + (tongHopDai.ChuaXuLyTuan) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7'>" + (tongHopDai.SoLuongTuan == 0 ? 0 : 100 * (tongHopDai.ChuaXuLyTuan) / (tongHopDai.SoLuongTuan)) + "%</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7'>" + (tongHopDai.SoLuongThang) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7'>" + (tongHopDai.ChuaXuLyThang) + "</td>" +
                                            "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7'>" + (tongHopDai.SoLuongThang == 0 ? 0 : 100 * (tongHopDai.ChuaXuLyThang) / (tongHopDai.SoLuongThang)) + "%</td>" +

                                         "</tr>";
                    }
                }
                #endregion

                #region TAO CONTENT EMAIL TABLE CANH BAO NGAY

                var listSite = new List<string>();

                var UT1SortTemp = UT1.OrderByDescending(_ => _.Tinh).ThenByDescending(_ => _.SoNgayDat).ToList();
                var UT2SortTemp = UT2.OrderByDescending(_ => _.Tinh).ThenByDescending(_ => _.SoNgayDat).ToList();
                var UT3SortTemp = UT3.OrderByDescending(_ => _.Tinh).ThenByDescending(_ => _.SoNgayDat).ToList();

                var UT1Sort = new List<SoCanhBaoUuTienTheoSite>();
                var UT2Sort = new List<SoCanhBaoUuTienTheoSite>();
                var UT3Sort = new List<SoCanhBaoUuTienTheoSite>();

                var dem = 0;
                for (var i = 0; i < UT1SortTemp.Count; i++)
                {
                    if (i == 0)
                    {
                        UT1Sort.Add(UT1SortTemp[i]);
                        dem++;
                    }
                    else if (UT1SortTemp[i].Tinh != UT1SortTemp[i - 1].Tinh)
                    {
                        dem = 0;
                        UT1Sort.Add(UT1SortTemp[i]);
                        dem++;
                    }
                    else
                    {
                        if (dem < 5)
                        {
                            UT1Sort.Add(UT1SortTemp[i]);
                            dem++;
                        }
                    }
                }
                dem = 0;
                for (var i = 0; i < UT2SortTemp.Count; i++)
                {
                    if (i == 0)
                    {
                        UT2Sort.Add(UT2SortTemp[i]);
                        dem++;
                    }
                    else if (UT2SortTemp[i].Tinh != UT2SortTemp[i - 1].Tinh)
                    {
                        dem = 0;
                        UT2Sort.Add(UT2SortTemp[i]);
                        dem++;
                    }
                    else
                    {
                        if (dem < 5)
                        {
                            UT2Sort.Add(UT2SortTemp[i]);
                            dem++;
                        }
                    }
                }
                dem = 0;
                for (var i = 0; i < UT3SortTemp.Count; i++)
                {
                    if (i == 0)
                    {
                        UT3Sort.Add(UT3SortTemp[i]);
                        dem++;
                    }
                    else if (UT3SortTemp[i].Tinh != UT3SortTemp[i - 1].Tinh)
                    {
                        dem = 0;
                        UT3Sort.Add(UT3SortTemp[i]);
                        dem++;
                    }
                    else
                    {
                        if (dem < 5)
                        {
                            UT3Sort.Add(UT3SortTemp[i]);
                            dem++;
                        }
                    }
                }

                var lengthMax = Math.Max(Math.Max(UT1Sort.Count, UT2Sort.Count), UT3Sort.Count);

                for (var i = 0; i < lengthMax; i++)
                {
                    var ut1Site = "";
                    var ut1SoLuongNhay = "";
                    var ut1SoNgayLienTuc = "";
                    var ut1SoLuongNhayTB = "";

                    var ut2Site = "";
                    var ut2SoLuongNhay = "";
                    var ut2SoNgayLienTuc = "";
                    var ut2SoLuongNhayTB = "";

                    var ut3Site = "";
                    var ut3SoLuongNhay = "";
                    var ut3SoNgayLienTuc = "";
                    var ut3SoLuongNhayTB = "";

                    if (i <= UT1Sort.Count - 1)
                    {
                        ut1Site = UT1Sort[i].Site;
                        ut1SoLuongNhay = UT1Sort[i].SoLuong.ToString();
                        ut1SoNgayLienTuc = UT1Sort[i].SoNgayDat.ToString();
                        ut1SoLuongNhayTB = ut1SoNgayLienTuc == "0" ? "0" : (UT1Sort[i].SoLuongCongDon / UT1Sort[i].SoNgayDat).ToString();
                        var siteId = ut1Site.Substring(0, 4);
                        if (!listSite.Any(t => t == siteId))
                        {
                            listSite.Add(siteId);
                        }

                    }
                    else
                    {
                        ut1Site = "";
                        ut1SoLuongNhay = "";
                        ut1SoNgayLienTuc = "";
                        ut1SoLuongNhayTB = "";
                    }

                    if (i <= UT2Sort.Count - 1)
                    {
                        ut2Site = UT2Sort[i].Site;
                        ut2SoLuongNhay = UT2Sort[i].SoLuong.ToString();
                        ut2SoNgayLienTuc = UT2Sort[i].SoNgayDat.ToString();
                        ut2SoLuongNhayTB = ut2SoNgayLienTuc == "0" ? "0" : (UT2Sort[i].SoLuongCongDon / UT2Sort[i].SoNgayDat).ToString();
                        var siteId = ut2Site.Substring(0, 4);
                        if (!listSite.Any(t => t == siteId))
                        {
                            listSite.Add(siteId);
                        }
                    }
                    else
                    {
                        ut2Site = "";
                        ut2SoLuongNhay = "";
                        ut2SoNgayLienTuc = "";
                        ut2SoLuongNhayTB = "";
                    }

                    if (i <= UT3Sort.Count - 1)
                    {
                        ut3Site = UT3Sort[i].Site;
                        ut3SoLuongNhay = UT3Sort[i].SoLuong.ToString();
                        ut3SoNgayLienTuc = UT3Sort[i].SoNgayDat.ToString();
                        ut3SoLuongNhayTB = ut3SoNgayLienTuc == "0" ? "0" : (UT3Sort[i].SoLuongCongDon / UT3Sort[i].SoNgayDat).ToString();
                        var siteId = ut3Site.Substring(0, 4);
                        if (!listSite.Any(t => t == siteId))
                        {
                            listSite.Add(siteId);
                        }
                    }
                    else
                    {
                        ut3Site = "";
                        ut3SoLuongNhay = "";
                        ut3SoNgayLienTuc = "";
                        ut3SoLuongNhayTB = "";
                    }

                    rowsOfTableSyncLost += "<tr>" +
                                   "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #dad4d9' >" + ut1Site + "</td>" +
                                   "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #FFF' >" + ut1SoNgayLienTuc + "</td>" +
                                   "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #FFF' >" + ut1SoLuongNhayTB + "</td>" +
                                   "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #FFF' >" + ut1SoLuongNhay + "</td>" +
                                   "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #dad4d9' >" + ut2Site + "</td>" +
                                   "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #FFF' >" + ut2SoNgayLienTuc + "</td>" +
                                   "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #FFF' >" + ut2SoLuongNhayTB + "</td>" +
                                   "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #FFF' >" + ut2SoLuongNhay + "</td>" +
                                   "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #dad4d9' >" + ut3Site + "</td>" +
                                   "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #FFF' >" + ut3SoNgayLienTuc + "</td>" +
                                   "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #FFF' >" + ut3SoLuongNhayTB + "</td>" +
                                   "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #FFF' >" + ut3SoLuongNhay + "</td>" +
                               "</tr>";
                }

                #region MAKE ATTACHMENT FILE

                string fileName = "Attachment_Template_File.xlsx";
                string fileNameDest = "Attachment_File_CanhBaoNgay_" + DateTime.Now.AddDays(-dayBefore).ToString("yyyyMMddHHmmss") + ".xlsx";

                string sourcePath = @"D:\SynLost\Default";
                string targetPath = @"D:\SynLost\Result";

                fileAttachmentPathSyncLost = @"D:\SynLost\Result\" + "Attachment_File_CanhBaoNgay_" + DateTime.Now.AddDays(-dayBefore).ToString("yyyyMMddHHmmss") + ".xlsx";
                var connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileAttachmentPathSyncLost + ";Extended Properties=\'Excel 12.0;HDR=Yes;TypeGuessRows=0;ImportMixedTypes=Text\'";


                // Use Path class to manipulate file and directory paths.
                string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                string destFile = System.IO.Path.Combine(targetPath, fileNameDest);

                // To copy a folder's contents to a new location:
                // Create a new target folder, if necessary.
                if (!System.IO.Directory.Exists(targetPath))
                {
                    System.IO.Directory.CreateDirectory(targetPath);
                }

                // To copy a file to another location and 
                // overwrite the destination file if it already exists.
                File.Copy(sourceFile, destFile, true);

                using (OleDbConnection conn = new OleDbConnection(connectionString))
                {
                    var command = new System.Data.OleDb.OleDbCommand();
                    string sql = null;
                    conn.Open();

                    command.Connection = conn;

                    var lengthMax2 = Math.Max(Math.Max(UT1SortTemp.Count, UT2SortTemp.Count), UT3SortTemp.Count);

                    for (var i = 0; i < lengthMax2; i++)
                    {
                        var ut1Site = "";
                        var ut1SoLuongNhay = "";
                        var ut1SoNgayLienTuc = "";
                        var ut1SoLuongNhayTB = "";

                        var ut2Site = "";
                        var ut2SoLuongNhay = "";
                        var ut2SoNgayLienTuc = "";
                        var ut2SoLuongNhayTB = "";

                        var ut3Site = "";
                        var ut3SoLuongNhay = "";
                        var ut3SoNgayLienTuc = "";
                        var ut3SoLuongNhayTB = "";

                        if (i <= UT1SortTemp.Count - 1)
                        {
                            ut1Site = UT1SortTemp[i].Site;
                            ut1SoLuongNhay = UT1SortTemp[i].SoLuong.ToString();
                            ut1SoNgayLienTuc = UT1SortTemp[i].SoNgayDat.ToString();
                            ut1SoLuongNhayTB = ut1SoNgayLienTuc == "0" ? "0" : (UT1SortTemp[i].SoLuongCongDon / UT1SortTemp[i].SoNgayDat).ToString();
                            var siteId = ut1Site.Substring(0, 4);
                            if (!listSite.Any(t => t == siteId))
                            {
                                listSite.Add(siteId);
                            }

                        }
                        else
                        {
                            ut1Site = "";
                            ut1SoLuongNhay = "";
                            ut1SoNgayLienTuc = "";
                            ut1SoLuongNhayTB = "";
                        }

                        if (i <= UT2SortTemp.Count - 1)
                        {
                            ut2Site = UT2SortTemp[i].Site;
                            ut2SoLuongNhay = UT2SortTemp[i].SoLuong.ToString();
                            ut2SoNgayLienTuc = UT2SortTemp[i].SoNgayDat.ToString();
                            ut2SoLuongNhayTB = ut2SoNgayLienTuc == "0" ? "0" : (UT2SortTemp[i].SoLuongCongDon / UT2SortTemp[i].SoNgayDat).ToString();
                            var siteId = ut2Site.Substring(0, 4);
                            if (!listSite.Any(t => t == siteId))
                            {
                                listSite.Add(siteId);
                            }
                        }
                        else
                        {
                            ut2Site = "";
                            ut2SoLuongNhay = "";
                            ut2SoNgayLienTuc = "";
                            ut2SoLuongNhayTB = "";
                        }

                        if (i <= UT3SortTemp.Count - 1)
                        {
                            ut3Site = UT3SortTemp[i].Site;
                            ut3SoLuongNhay = UT3SortTemp[i].SoLuong.ToString();
                            ut3SoNgayLienTuc = UT3SortTemp[i].SoNgayDat.ToString();
                            ut3SoLuongNhayTB = ut3SoNgayLienTuc == "0" ? "0" : (UT3SortTemp[i].SoLuongCongDon / UT3SortTemp[i].SoNgayDat).ToString();
                            var siteId = ut3Site.Substring(0, 4);
                            if (!listSite.Any(t => t == siteId))
                            {
                                listSite.Add(siteId);
                            }
                        }
                        else
                        {
                            ut3Site = "";
                            ut3SoLuongNhay = "";
                            ut3SoNgayLienTuc = "";
                            ut3SoLuongNhayTB = "";
                        }

                        sql = "Insert into [Sheet1$] values('" + ut1Site + "','" + ut1SoNgayLienTuc + "','" + ut1SoLuongNhayTB + "','" + ut1SoLuongNhay + "','" + ut2Site + "','" + ut2SoNgayLienTuc + "','" + ut2SoLuongNhayTB + "','" + ut2SoLuongNhay + "','" + ut3Site + "','" + ut3SoNgayLienTuc + "','" + ut3SoLuongNhayTB + "','" + ut3SoLuongNhay + "')";
                        command.CommandText = sql;
                        command.ExecuteNonQuery();
                    }

                }
                #endregion

                #endregion

                #region TAO CONTENT EMAIL TABLE XU_LY_OK NGAY
                lengthMax = XuLyOkNgaySyncLost.Count;

                for (var i = 0; i < lengthMax; i++)
                {
                    var site = "";
                    var ngayBatDau = "";
                    var ngayXuLy = "";
                    var tinh = "";
                    var dvt = "";
                    var soNgayKeoDai = "";
                    if (i <= XuLyOkNgaySyncLost.Count - 1)
                    {
                        site = XuLyOkNgaySyncLost[i].Site;
                        ngayBatDau = XuLyOkNgaySyncLost[i].Ngay;
                        ngayXuLy = XuLyOkNgaySyncLost[i].NgayXuLy;
                        tinh = XuLyOkNgaySyncLost[i].Tinh;
                        dvt = XuLyOkNgaySyncLost[i].DVT;
                        soNgayKeoDai = XuLyOkNgaySyncLost[i].SoNgayKeoDai.ToString();
                    }
                    else
                    {
                        site = "";
                        ngayBatDau = "";
                        ngayXuLy = "";
                        tinh = "";
                        dvt = "";
                        soNgayKeoDai = "";
                    }

                    rowsOfTableSyncLost3 += "<tr>" +
                                   "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #e06868' >" + dvt + "</td>" +
                                   "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #e06868' >" + tinh + "</td>" +
                                   "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #e06868' >" + site + "</td>" +
                                   "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #e06868' >" + ngayBatDau + "</td>" +
                                   "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #e06868' >" + ngayXuLy + "</td>" +
                                   "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #e06868' >" + soNgayKeoDai + "</td>" +
                               "</tr>";
                }

                #endregion
            }
            catch (Exception ex)
            {
                //MessageBox.Show("err: " + ex.Message);
                lblError.Text = "Date: " + DateTime.Now + ". Er: " + ex.Message;
            }
        }

        private void Get_Report_KPI()
        {
            TramPhatSinhThang = new List<XuLyOkTheoSite>();

            var queryReportThang = "select TRAM from LICHSUCANHBAO"
                                     + " where Convert(date,SUBSTRING(CASE_ALARM,14,4)+ '-'+ SUBSTRING(CASE_ALARM,11,2) +'-'+SUBSTRING(CASE_ALARM,8,2)) >='" + DateTime.Now.AddMonths(-1).ToString("MM") + "/01/2017'"
                                     + " and Convert(date,SUBSTRING(CASE_ALARM,14,4)+ '-'+ SUBSTRING(CASE_ALARM,11,2) +'-'+SUBSTRING(CASE_ALARM,8,2)) < '" + DateTime.Now.ToString("MM") + "/01/2017'"
                                     + " group by TRAM"
                                     + " order by TRAM";

            var dtTramPhatSinhThang = new DataTable();
            dtTramPhatSinhThang = dbSyncLost.SelectQuery(queryReportThang);

            foreach (DataRow dr in dtTramPhatSinhThang.Rows)
            {
                var item = new XuLyOkTheoSite();
                item.Site = dr["TRAM"].ToString();

                item.Tinh = item.Site.Substring(0, 2);

                if (item.Tinh == "QT" || item.Tinh == "HU" || item.Tinh == "DN" || item.Tinh == "QA")
                {
                    switch (item.Tinh)
                    {
                        case "QT":
                            item.Tinh = "Quang Tri";
                            break;
                        case "HU":
                            item.Tinh = "TT-HUE";
                            break;
                        case "DN":
                            item.Tinh = "Da Nang";
                            break;
                        case "QA":
                            item.Tinh = "Quang Nam";
                            break;
                        default: break;
                    }
                    item.DVT = "ĐVTĐN";
                }
                else if (item.Tinh == "QN" || item.Tinh == "PY" || item.Tinh == "BD" || item.Tinh == "KH")
                {
                    switch (item.Tinh)
                    {
                        case "QN":
                            item.Tinh = "Quang Ngai";
                            break;
                        case "PY":
                            item.Tinh = "Phu Yen";
                            break;
                        case "BD":
                            item.Tinh = "Binh Dinh";
                            break;
                        case "KH":
                            item.Tinh = "Khanh Hoa";
                            break;
                        default: break;
                    }
                    item.DVT = "ĐVTBĐ";
                }
                else if (item.Tinh == "GL" || item.Tinh == "KT" || item.Tinh == "DL" || item.Tinh == "DG")
                {
                    switch (item.Tinh)
                    {
                        case "GL":
                            item.Tinh = "Gia Lai";
                            break;
                        case "KT":
                            item.Tinh = "Kon Tum";
                            break;
                        case "DL":
                            item.Tinh = "Dak Lak";
                            break;
                        case "DG":
                            item.Tinh = "Dac Nong";
                            break;
                        default: break;
                    }
                    item.DVT = "ĐVTĐL";
                }

                TramPhatSinhThang.Add(item);
            }

            var rowTableKPI1 = "";
            for (var i = 0; i < TramPhatSinhThang.Count; i++)
            {
                var site = "";
                var dvt = "";
                site = TramPhatSinhThang[i].Site;
                dvt = TramPhatSinhThang[i].DVT;
                var tinh = TramPhatSinhThang[i].Tinh;

                rowTableKPI1 += "<tr>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #fff' >" + dvt + "</td>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #fff' >" + tinh + "</td>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #fff' >" + site + "</td>" +
                           "</tr>";
            }

            #region MAKE ATTACHMENT FILE

            string fileName = "Template_Thang.xlsx";
            string fileNameDest = "Attachment_File_Thang_" + DateTime.Now.AddMonths(-1).ToString("yyyyMMddHHmmss") + ".xlsx";

            string sourcePath = @"D:\SynLost\Default";
            string targetPath = @"D:\SynLost\Result";

            var fileAttachmentPath = @"D:\SynLost\Result\" + "Attachment_File_Thang_" + DateTime.Now.AddMonths(-1).ToString("yyyyMMddHHmmss") + ".xlsx";
            var connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileAttachmentPath + ";Extended Properties=\'Excel 12.0;HDR=Yes;TypeGuessRows=0;ImportMixedTypes=Text\'";


            // Use Path class to manipulate file and directory paths.
            string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
            string destFile = System.IO.Path.Combine(targetPath, fileNameDest);

            // To copy a folder's contents to a new location:
            // Create a new target folder, if necessary.
            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }

            // To copy a file to another location and 
            // overwrite the destination file if it already exists.
            File.Copy(sourceFile, destFile, true);

            using (OleDbConnection conn = new OleDbConnection(connectionString))
            {
                var command = new System.Data.OleDb.OleDbCommand();
                string sql = null;
                conn.Open();

                command.Connection = conn;

                for (var i = 0; i < TramPhatSinhThang.Count; i++)
                {
                    var site = TramPhatSinhThang[i].Site;
                    var dvt = TramPhatSinhThang[i].DVT;
                    var tinh = TramPhatSinhThang[i].Tinh;

                    sql = "Insert into [Sheet1$] values('" + dvt + "','" + tinh + "','" + site + "')";
                    command.CommandText = sql;
                    command.ExecuteNonQuery();
                }

            }
            #endregion

            var queryReport3ngay = "Select TINH, TRAM, SUBSTRING(CASE_ALARM,8,10) as NGAY,"
                                    + " FORMAT (NGAY_XU_LY, 'd', 'en-gb' ) as NGAY_XU_LY, "
                                    + " DATEDIFF(d,Convert(date,SUBSTRING(CASE_ALARM,14,4)+ '-'+ SUBSTRING(CASE_ALARM,11,2) +'-'+SUBSTRING(CASE_ALARM,8,2)),NGAY_XU_LY) as SONGAYKEODAI"
                                    + " from LICHSUCANHBAO "
                                    + " where NGAY_XU_LY >= '" + DateTime.Now.AddMonths(-1).ToString("MM") + "/1/2017' and NGAY_XU_LY < '" + DateTime.Now.ToString("MM") + "/10/2017' and DA_XU_LY = 1 and MUC_UU_TIEN > 0 "
                                    + " and DATEDIFF(d,Convert(date,SUBSTRING(CASE_ALARM,14,4)+ '-'+ SUBSTRING(CASE_ALARM,11,2) +'-'+SUBSTRING(CASE_ALARM,8,2)),NGAY_XU_LY) <= 3"
                                    + " GROUP BY TINH, TRAM, NGAY_XU_LY,CASE_ALARM";

            Tram3NgayKPI = new List<XuLyOkTheoSite>();
            var dtTram3NgayKPI = new DataTable();
            dtTram3NgayKPI = dbSyncLost.SelectQuery(queryReport3ngay);

            foreach (DataRow dr in dtTram3NgayKPI.Rows)
            {
                var item = new XuLyOkTheoSite();
                item.Site = dr["TRAM"].ToString();

                item.Tinh = item.Site.Substring(0, 2);

                if (item.Tinh == "QT" || item.Tinh == "HU" || item.Tinh == "DN" || item.Tinh == "QA")
                {
                    switch (item.Tinh)
                    {
                        case "QT":
                            item.Tinh = "Quang Tri";
                            break;
                        case "HU":
                            item.Tinh = "TT-HUE";
                            break;
                        case "DN":
                            item.Tinh = "Da Nang";
                            break;
                        case "QA":
                            item.Tinh = "Quang Nam";
                            break;
                        default: break;
                    }
                    item.DVT = "ĐVTĐN";
                }
                else if (item.Tinh == "QN" || item.Tinh == "PY" || item.Tinh == "BD" || item.Tinh == "KH")
                {
                    switch (item.Tinh)
                    {
                        case "QN":
                            item.Tinh = "Quang Ngai";
                            break;
                        case "PY":
                            item.Tinh = "Phu Yen";
                            break;
                        case "BD":
                            item.Tinh = "Binh Dinh";
                            break;
                        case "KH":
                            item.Tinh = "Khanh Hoa";
                            break;
                        default: break;
                    }
                    item.DVT = "ĐVTBĐ";
                }
                else if (item.Tinh == "GL" || item.Tinh == "KT" || item.Tinh == "DL" || item.Tinh == "DG")
                {
                    switch (item.Tinh)
                    {
                        case "GL":
                            item.Tinh = "Gia Lai";
                            break;
                        case "KT":
                            item.Tinh = "Kon Tum";
                            break;
                        case "DL":
                            item.Tinh = "Dak Lak";
                            break;
                        case "DG":
                            item.Tinh = "Dac Nong";
                            break;
                        default: break;
                    }
                    item.DVT = "ĐVTĐL";
                }

                Tram3NgayKPI.Add(item);
            }
            var rowTableKPI2 = "";
            for (var i = 0; i < Tram3NgayKPI.Count; i++)
            {
                var site = "";
                var dvt = "";
                site = Tram3NgayKPI[i].Site;
                dvt = Tram3NgayKPI[i].DVT;
                var tinh = Tram3NgayKPI[i].Tinh;
                rowTableKPI2 += "<tr>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #fff' >" + dvt + "</td>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #fff' >" + tinh + "</td>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #fff' >" + site + "</td>" +
                           "</tr>";
            }

            #region MAKE ATTACHMENT FILE

            fileName = "Template_3ngay.xlsx";
            fileNameDest = "Attachment_File_3Ngay_" + DateTime.Now.AddMonths(-1).ToString("yyyyMMddHHmmss") + ".xlsx";

            sourcePath = @"D:\SynLost\Default";
            targetPath = @"D:\SynLost\Result";

            var fileAttachmentPath2 = @"D:\SynLost\Result\" + "Attachment_File_3Ngay_" + DateTime.Now.AddMonths(-1).ToString("yyyyMMddHHmmss") + ".xlsx";
            connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileAttachmentPath2 + ";Extended Properties=\'Excel 12.0;HDR=Yes;TypeGuessRows=0;ImportMixedTypes=Text\'";


            // Use Path class to manipulate file and directory paths.
            sourceFile = System.IO.Path.Combine(sourcePath, fileName);
            destFile = System.IO.Path.Combine(targetPath, fileNameDest);

            // To copy a folder's contents to a new location:
            // Create a new target folder, if necessary.
            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }

            // To copy a file to another location and 
            // overwrite the destination file if it already exists.
            File.Copy(sourceFile, destFile, true);

            using (OleDbConnection conn = new OleDbConnection(connectionString))
            {
                var command = new System.Data.OleDb.OleDbCommand();
                string sql = null;
                conn.Open();

                command.Connection = conn;

                for (var i = 0; i < Tram3NgayKPI.Count; i++)
                {
                    var site = Tram3NgayKPI[i].Site;
                    var dvt = Tram3NgayKPI[i].DVT;
                    var tinh = Tram3NgayKPI[i].Tinh;

                    sql = "Insert into [Sheet1$] values('" + dvt + "','" + tinh + "','" + site + "')";
                    command.CommandText = sql;
                    command.ExecuteNonQuery();
                }

            }
            #endregion

            var bodyMail = "<html>" +
                             "<body>" +
                                 "<div style='padding-left: 10px;margin: 20px; font-size: 12px;'>" +
                                 "<div style='color: #000; font-size: 12px;'>Dear anh/chị,</div><br><br>" +

                                 "<div style='color: #000; font-size: 12px;'> Đây là bảng tổng hợp trạm có phát sinh cảnh báo Sync Lost tháng " + DateTime.Now.AddMonths(-1).ToString("dd/MM/yyyy") + ".</div>" +

                                 "<table style='border: 1px solid black; border-collapse: collapse; margin-top: 10px; margin-left: 10px; width: 100%;'>" +
                                    "<tbody>" +
                                        "<tr>" +
                                            "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #98b2e4'>Đài</th>" +
                                            "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #bdbc80'>Tỉnh</th>" +
                                            "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #da5b29'>Site</th>" +
                                        "</tr>" +

                                         rowTableKPI1 +
                                    "</tbody>" +
                                    "</table><br>" +

                                 "<div><br>Bảng tổng hợp những trạm xử lý Ok synclost trong vòng 3 ngày kể từ lúc phát sinh: <br></div>" +

                                 "<table style='border: 1px solid black; border-collapse: collapse; margin-top: 10px; margin-left: 10px; width: 100%'>" +
                                    "<tbody>" +
                                        "<tr>" +
                                            "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #98b2e4'>Đài</th>" +
                                            "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #bdbc80'>Tỉnh</th>" +
                                            "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #da5b29'>Site</th>" +
                                        "</tr>" +

                                         rowTableKPI2 +
                                    "</tbody>" +
                                    "</table>" +

                                    "<div style='color: #000; font-size: 12px;'><br><br>Best Regard!</div>" +
                                    "<div style='color: #000; font-size: 12px;'>TTMLMT</div>" +
                                    "</div>" +
                              "</body>" +
                        "</html>";

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress("mlmt.cntt@mobifone.vn", "no-reply@mobifone.vn");
            mail.To.Add("mbftunt@gmail.com");
            if (radioButton_yn_EmailTest_No.Checked == true)
            {
                var listMailAdd = GetListMail();
                for (var i = 0; i < listMailAdd.Count; i++)
                {
                    mail.To.Add(listMailAdd[i]);
                }
            }
            mail.Subject = "Báo cáo Sync Lost KPI tháng" + DateTime.Now.AddMonths(-1).ToString("MM/yyyy");
            mail.IsBodyHtml = true;

            //Trạm đưa vào danh sách xử lý: trạm có sync lost nháy từ 10 lần/ngày trở lên.
            //Trạm đưa ra ngoài danh sách xử lý: trạm có sync lost < 10 lần/ngày trong 3 ngày liên tục.

            mail.Body = bodyMail;
            var attachment = new System.Net.Mail.Attachment(fileAttachmentPath);
            mail.Attachments.Add(attachment);
            var attachment2 = new System.Net.Mail.Attachment(fileAttachmentPath2);
            mail.Attachments.Add(attachment2);
            var tryAgain = 10;
            var failed = false;
            do
            {
                try
                {
                    failed = false;
                    code.SmtpClient().Send(mail);
                }
                catch (Exception ex) // I would avoid catching all exceptions equally, but ymmv
                {
                    failed = true;
                    tryAgain--;
                    var exception = ex.Message.ToString();
                    //Other code for saving exception message to a log.
                    Console.WriteLine("Date: " + DateTime.Now + ". Er: " + ex.Message);
                    //MessageBox.Show("Date: " + DateTime.Now + ". Er: " + ex.Message);
                    lblError.Text = "Date: " + DateTime.Now + ". Er: " + ex.Message;
                }
            } while (failed && tryAgain != 0);

            if (!failed)
            {
                lblError.Text = "";
            }
            rowTableKPI1 = "";
            rowTableKPI2 = "";
        }

        private void SendEmail()
        {
            #region Build Chart Synclost
            var listValue = new List<Value7Days>();
            var queryValueDate = "select * from SOLIEUCASEALARM where NGAY < '" + DateTime.Now.ToString("MM-dd-yyyy") + "' and NGAY >= '" + DateTime.Now.AddDays(-7).ToString("MM-dd-yyyy") + "' order by TINH,NGAY";

            var dbValueDate = dbSyncLost.SelectQuery(queryValueDate);
            var value7Days = new Value7Days();

            for (var i = 0; i < dbValueDate.Rows.Count; i++)
            {
                if (dbValueDate.Rows[i]["TINH"].ToString() != value7Days.Tinh)
                {
                    if (!string.IsNullOrEmpty(value7Days.Tinh))
                    {
                        listValue.Add(value7Days);
                    }

                    value7Days = new Value7Days();
                    value7Days.DateValues = new List<DateValue>();
                    value7Days.Tinh = dbValueDate.Rows[i]["TINH"].ToString();
                    var dateValue = new DateValue();
                    dateValue.SDate = Convert.ToDateTime(dbValueDate.Rows[i]["NGAY"].ToString()).ToString("dd-MM-yyyy");
                    dateValue.Value = Convert.ToInt32(dbValueDate.Rows[i]["SO_CASE_OPEN"].ToString());
                    value7Days.DateValues.Add(dateValue);
                }
                else
                {
                    var dateValue = new DateValue();
                    dateValue.SDate = Convert.ToDateTime(dbValueDate.Rows[i]["NGAY"].ToString()).ToString("dd-MM-yyyy");
                    dateValue.Value = Convert.ToInt32(dbValueDate.Rows[i]["SO_CASE_OPEN"].ToString());
                    value7Days.DateValues.Add(dateValue);
                }

                if (i == dbValueDate.Rows.Count - 1)
                {
                    listValue.Add(value7Days);
                }
            }


            var listContentId = new List<string>();

            for (var i = 0; i < 1; i++)
            {
                var ten = "Biểu đồ cảnh báo syclost 7 ngày gần nhất";
                bodyChartSyncLost += @"<div>" + ten + @": <br/> <img style='' title='" + ten + @"' src=""$CONTENTID" + (i + 1) + @"$""/></div>";
            }

            for (var i = 1; i <= 1; i++)
            {
                var CONTENTID1 = Guid.NewGuid().ToString().Replace("-", "");
                bodyChartSyncLost = bodyChartSyncLost.Replace("$CONTENTID" + i + "$", "cid:" + CONTENTID1);
                listContentId.Add(CONTENTID1);
            }

            #region TUNT - Create body mail
            var bodyMail = "<html>" +
                                 "<body>" +
                                     "<div style='padding-left: 10px;margin: 20px; font-size: 12px;'>" +
                                     "<div style='color: #000; font-size: 12px;'>Dear anh/chị,</div><br><br>" +
                                     "<div style='color: #000; font-size: 12px;'> Đây là bảng tổng hợp thông tin Sync Lost ngày " + DateTime.Now.AddDays(-dayBefore).ToString("dd/MM/yyyy") + ".</div>" +
                                     "<div style='color: #000; font-size: 12px;'> <br>- Trạm đưa vào danh sách xử lý: trạm có sync lost nháy từ 10 lần/ngày trở lên.</div>" +
                                     "<div style='color: #000; font-size: 12px;'> - Trạm đưa ra ngoài danh sách xử lý: trạm có sync lost < 10 lần/ngày trong 3 ngày liên tục.</div>" +
                                     "<table style='border: 1px solid black; border-collapse: collapse; margin-top: 10px; margin-left: 10px; width: 100%;'>" +
                                        "<tbody>" +
                                            "<tr>" +
                                                "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #98b2e4' rowspan='2'>Đài</th>" +
                                                "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #bdbc80' rowspan='2'>Tỉnh</th>" +
                                                "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #eac611' colspan='4' >Cảnh báo ngày<br></th>" +
                                                "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #0de3f9' colspan='3'>Lũy kế tuần<br>" + startDayOfWeek.ToString("dd//MM/yyyy") + " - " + startDayOfWeek.AddDays(6).ToString("dd/MM/yyyy") + "</th>" +
                                                "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #6c75e6' colspan='3'>Lũy kế tháng " + DateTime.Now.AddDays(-dayBefore).ToString("MM/yyyy") + "</th>" +
                                            "</tr>" +
                                            "<tr>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #eac611' > > 5 ngày</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #eac611' >3-5 ngày </td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #eac611' > < 3 ngày </td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #eac611' >Tổng số trạm</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #0de3f9' >Số lượng</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #0de3f9' >Chưa xử lý</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #0de3f9' >Tỉ lệ</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #6c75e6' >Số lượng</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #6c75e6' >Chưa xử lý</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #6c75e6' >Tỉ lệ</td>" +

                                             "</tr>" +
                                             rowsOfTableSyncLost2 +
                                        "</tbody>" +
                                        "</table><br>" +
                                        bodyChartSyncLost +
                                     "<div><br>Bảng cảnh báo ngày:<br></div>" +
                                     "<div><br>  Chỉ hiển thị top 5 trạm có số ngày bị cảnh báo cao nhất của mỗi tỉnh. Chi tiết xem file đính kèm.<br></div>" +
                                     "<table style='border: 1px solid black; border-collapse: collapse; margin-top: 10px; margin-left: 10px; width: 100%'>" +
                                        "<tbody>" +
                                            "<tr>" +
                                                "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #e06868' colspan='4'> > 5 ngày</th>" +
                                                "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #6c75e6' colspan='4'>3-5 ngày</th>" +
                                                "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #0de3f9' colspan='4'> < 3 ngày</th>" +
                                            "</tr>" +
                                            "<tr>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #e06868' >Tên trạm</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #e06868' >Số ngày</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #e06868' >Số nháy<br>trung bình/ ngày</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #e06868' >Số nháy trong ngày</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #6c75e6' >Tên trạm</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #6c75e6' >Số ngày</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #6c75e6' >Số nháy<br>trung bình/ ngày</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #6c75e6' >Số nháy trong ngày</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #0de3f9' >Tên trạm</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #0de3f9' >Số ngày</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #0de3f9' >Số nháy<br>trung bình/ ngày</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #0de3f9' >Số nháy trong ngày</td>" +
                                             "</tr>" +
                                             rowsOfTableSyncLost +
                                        "</tbody>" +
                                        "</table>" +
                                        "<div><br>Bảng xử lý OK ngày<br></div>" +
                                        "<table style='border: 1px solid black; border-collapse: collapse; margin-top: 10px; margin-left: 10px; width: 100%'>" +
                                        "<tbody>" +
                                            "<tr>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #e06868' >Đài</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #e06868' >Tỉnh</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #e06868' >Tên trạm</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #e06868' >Ngày bắt đầu</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #e06868' >Ngày xử lý OK</td>" +
                                                "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; font-weight: bold; background: #e06868' >Số ngày kéo dài</td>" +
                                             "</tr>" +
                                             rowsOfTableSyncLost3 +
                                        "</tbody>" +
                                        "</table><br>" +
                                        "<div style='color: #000; font-size: 12px;'><br><br>Best Regard!</div>" +
                                        "<div style='color: #000; font-size: 12px;'>TTMLMT</div>" +
                                        "</div>" +
                                  "</body>" +
                            "</html>";
            #endregion

            htmlViewSyncLost = AlternateView.CreateAlternateViewFromString(bodyMail, null, MediaTypeNames.Text.Html);


            for (var i = 0; i < 1; i++) //Tuy bien i de tao nhieu bieu do
            {
                chart1.Series.Clear();
                if (chart1.Titles.Count == 0)
                {
                    var ten = "MLMT";
                    chart1.Titles.Add("BIỂU ĐỒ SYNCLOST  " + ten);
                }
                else
                {
                    chart1.Titles.RemoveAt(0);
                    var ten = "MLMT";
                    chart1.Titles.Add("BIỂU ĐỒ SYNCLOST  " + ten);
                }

                double min = 0;
                double max = 20;

                //build chart cho moi tram
                for (var j = 0; j < listValue[0].DateValues.Count; j++)
                {
                    var sDate = listValue[0].DateValues[j].SDate;

                    var series = chart1.Series.Add(sDate);
                    series.ChartType = SeriesChartType.Column;

                    areaAxisSyncLost.AxisX.MajorGrid.Enabled = false;
                    areaAxisSyncLost.AxisX.MajorTickMark.Enabled = false;
                    areaAxisSyncLost.AxisX.LabelStyle.Enabled = false;
                    areaAxisSyncLost.AxisY.MajorGrid.Enabled = false;
                    areaAxisSyncLost.AxisY.MajorTickMark.Enabled = false;
                    areaAxisSyncLost.AxisY.LabelStyle.Enabled = false;

                    areaAxisSyncLost.AxisX.Title = "Giờ";
                    areaAxisSyncLost.AxisY.Title = "Giá trị";
                    for (var k = 0; k < 12; k++)
                    {
                        if (max < listValue[k].DateValues[j].Value) max = listValue[k].DateValues[j].Value;
                        if (min > listValue[k].DateValues[j].Value) min = listValue[k].DateValues[j].Value;
                        series.Points.AddXY(listValue[k].Tinh, listValue[k].DateValues[j].Value);
                    }

                    chart1.ChartAreas[0].AxisX.Interval = 1;

                    chart1.ChartAreas[0].AxisY.Maximum = Math.Round(max, 0) + 5;
                    chart1.ChartAreas[0].AxisY.Minimum = Math.Round(min, 0);
                }

                var stream = new MemoryStream();
                chart1.SaveImage(stream, System.Drawing.Imaging.ImageFormat.Png);
                stream.Position = 0;
                LinkedResource imagelink1 = new LinkedResource(stream, "image/png");
                imagelink1.ContentId = listContentId[i];
                imagelink1.ContentType.Name = "Bieu Do";
                imagelink1.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                htmlViewSyncLost.LinkedResources.Add(imagelink1);
            }
            #endregion

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress("mlmt.cntt@mobifone.vn", "no-reply@mobifone.vn");
            mail.To.Add("mbftunt@gmail.com");
            if (radioButton_yn_EmailTest_No.Checked == true)
            {
                var listMailAdd = GetListMail();
                for (var i = 0; i < listMailAdd.Count; i++)
                {
                    mail.To.Add(listMailAdd[i]);
                }
            }
            mail.Subject = "Báo cáo Sync Lost " + DateTime.Now.AddDays(-dayBefore).ToString("dd/MM/yyyy");
            mail.IsBodyHtml = true;

            //Trạm đưa vào danh sách xử lý: trạm có sync lost nháy từ 10 lần/ngày trở lên.
            //Trạm đưa ra ngoài danh sách xử lý: trạm có sync lost < 10 lần/ngày trong 3 ngày liên tục.

            mail.Body = bodyMail;
            mail.AlternateViews.Add(htmlViewSyncLost);
            var attachment = new System.Net.Mail.Attachment(fileAttachmentPathSyncLost);
            mail.Attachments.Add(attachment);

            //SmtpClient smtp = new SmtpClient("10.3.12.32", 25);
            //smtp.EnableSsl = false;
            //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            //smtp.Credentials = new NetworkCredential("mlmt.cntt@mobifone.vn", "abcd@1234");
            //smtp.Timeout = 20000;

            var tryAgain = 10;
            var failed = false;
            do
            {
                try
                {
                    failed = false;
                    __.SmtpClient().Send(mail);
                    __.WriteToRichTextBox(richMessage,"Send Mail Get_SynLost");
                }
                catch (Exception ex) // I would avoid catching all exceptions equally, but ymmv
                {
                    failed = true;
                    tryAgain--;
                    var exception = ex.Message.ToString();
                    //Other code for saving exception message to a log.
                    Console.WriteLine("Date: " + DateTime.Now + ". Er: " + ex.Message);
                    //MessageBox.Show("Date: " + DateTime.Now + ". Er: " + ex.Message);
                    lblError.Text = "Date: " + DateTime.Now + ". Er: " + ex.Message;
                }
            } while (failed && tryAgain != 0);

            if (!failed)
            {
                lblError.Text = "";
            }
            rowsOfTableSyncLost = "";
            rowsOfTableSyncLost2 = "";
            rowsOfTableSyncLost3 = "";
            bodyChartSyncLost = "";
        }

        private void timerSendEmail_Tick(object sender, EventArgs e)
        {
            var currenttime = System.DateTime.Now.ToString("HH:mm tt");
            //tunt chỉnh 08 = 05 
            var _timerAutoMail = txtTimerAutoMailSynLost.Text;
            if (currenttime.Substring(0, 2) == _timerAutoMail.Substring(0,2) && currenttime.Substring(3, 2) == _timerAutoMail.Substring(3,2) && !IsBusy)
            {
                //get du lieu, tao noi dung mail
                __.WriteToRichTextBox(richMessage, "Run at timerSendEmail_Tick ");
                Get_SynLost();

                // send email
                SendEmail();//đúng 8h 25 sẽ vào tổng hợp báo cáo và gửi mail

                //nếu sau khi thực hiện quá trình báo cáo mail xong, mà thời gian qua 8h26 thì set busy=false
                //vì nếu có vào lại timer thì cũng ko thỏa đc phút = 25 mà thực hiện quá trihf báo cáo, mail
                //còn nếu thực hiện xong mà vẫn 8h25 thì ko cần làm gì cả, vì bây giwof isbusy vẫn bằng true
                // đến 9h thì mới mở lại isbusy = false
                while (currenttime.Substring(3, 2) != _timerAutoMail.Substring(3, 2))
                {
                    currenttime = System.DateTime.Now.ToString("HH:mm tt");
                    IsBusy = false;
                }
            }
            else if (currenttime.Substring(0, 2) == "09")
            {
                IsBusy = false;
            }
            var _tuntTimerAutoMailMPD = txtTimerAutoMailMPD.Text;
            if (currenttime.Substring(0, 2) == _tuntTimerAutoMailMPD.Substring(0, 2) && currenttime.Substring(3, 2) == _tuntTimerAutoMailMPD.Substring(3, 2) && !IsBusyMPD)
            {
                IsBusyMPD = true;
                MayPhatDienAlarm();
                __.WriteToRichTextBox(richMessage,"Test MPD");
                while (currenttime.Substring(3, 2) != _tuntTimerAutoMailMPD.Substring(3, 2))
                {
                    currenttime = System.DateTime.Now.ToString("HH:mm tt");
                    IsBusyMPD = false;
                }
            }
            else if (currenttime.Substring(0, 2) == _tuntTimerAutoMailMPD.Substring(0, 2) && currenttime.Substring(3, 2) == Convert.ToInt32(_tuntTimerAutoMailMPD.Substring(3, 2) + 1).ToString())
            {
                IsBusyMPD = false;
            }
        }

        private void TestReport()
        {
            var UT1 = new List<SoCanhBaoUuTienTheoSite>();
            var UT2 = new List<SoCanhBaoUuTienTheoSite>();
            var UT3 = new List<SoCanhBaoUuTienTheoSite>();

            #region GET DATA CANH BAO NGAY TONG HOP
            //Data ngay Tong hop
            var query = "Select TINH,TRAM, CASE_ALARM, COUNT(NGAY) as SO_NGAY, SUM(SO_LAN_NHAY) as TONG_SO_NHAY" +
                      " from LICHSUCANHBAO where DA_XU_LY = 0 and MUC_UU_TIEN > 0 and NGAY < '" + DateTime.Now.AddDays(-dayBefore + 1).ToString("MM/dd/yyyy") + "' " +
                      "GROUP BY TINH,CASE_ALARM, TRAM order by TINH";
            var dtCanhBaoNgay = new DataTable();
            dtCanhBaoNgay = dbSyncLost.SelectQuery(query);
            var listCanhBaoNgayTongHop = new List<CanhBaoTongHop>();
            var cb = new CanhBaoTongHop();
            var countRow = 0;
            foreach (DataRow dr in dtCanhBaoNgay.Rows)
            {
                countRow++;
                var itemUT = new SoCanhBaoUuTienTheoSite();
                itemUT.Site = dr["TRAM"].ToString();
                itemUT.Tinh = dr["TINH"].ToString();
                itemUT.SoLuongCongDon = Convert.ToInt32(dr["TONG_SO_NHAY"]);
                itemUT.SoNgayDat = Convert.ToInt32(dr["SO_NGAY"]);

                if (cb.Tinh != dr["TINH"].ToString())
                {
                    if (!string.IsNullOrEmpty(cb.Tinh))
                    {
                        listCanhBaoNgayTongHop.Add(cb);
                        cb = new CanhBaoTongHop();
                    }
                    cb.TongSoNhay += itemUT.SoLuongCongDon;
                    cb.Tinh = dr["TINH"].ToString();
                    cb.DVT = "KXD";

                    if (cb.Tinh == "Quang Tri" || cb.Tinh == "TT-Hue" || cb.Tinh == "Da Nang" || cb.Tinh == "Quang Nam")
                    {
                        cb.DVT = "ĐVTĐN";
                    }
                    else if (cb.Tinh == "Quang Ngai" || cb.Tinh == "Phu Yen" || cb.Tinh == "Binh Dinh" || cb.Tinh == "Khanh Hoa")
                    {
                        cb.DVT = "ĐVTBĐ";
                    }
                    else if (cb.Tinh == "Gia Lai" || cb.Tinh == "Kon Tum" || cb.Tinh == "Dak Lak" || cb.Tinh == "Dac Nong")
                    {
                        cb.DVT = "ĐVTĐL";
                    }
                    var soLuongLienTuc = Convert.ToInt32(dr["SO_NGAY"]);

                    if (soLuongLienTuc > 5)
                    {
                        cb.UT1++; //Nhom 1
                        UT1.Add(itemUT);
                    }
                    else if (soLuongLienTuc > 3)
                    {
                        cb.UT2++; //Nhom 2
                        UT2.Add(itemUT);
                    }
                    else
                    {
                        cb.UT3++; //Nhom 3
                        UT3.Add(itemUT);
                    }

                    if (countRow == dtCanhBaoNgay.Rows.Count)
                    {
                        listCanhBaoNgayTongHop.Add(cb);
                    }
                }
                else
                {
                    cb.TongSoNhay += itemUT.SoLuongCongDon;
                    var soLuongLienTuc = Convert.ToInt32(dr["SO_NGAY"]);

                    if (soLuongLienTuc > 5)
                    {
                        cb.UT1++; //Nhom 1
                        UT1.Add(itemUT);
                    }
                    else if (soLuongLienTuc > 3)
                    {
                        cb.UT2++; //Nhom 2
                        UT2.Add(itemUT);
                    }
                    else
                    {
                        cb.UT3++; //Nhom 3
                        UT3.Add(itemUT);
                    }
                    if (countRow == dtCanhBaoNgay.Rows.Count)
                    {
                        listCanhBaoNgayTongHop.Add(cb);
                    }
                }


                //var mucUT = Convert.ToInt32(dr["MUC_UU_TIEN"]);

                //if (!(itemUT.Site[0] == 'B' && char.IsLetter(itemUT.Site[4])) && (itemUT.Site[0] != 'N' && itemUT.Site[1] != 'A'))
                //{
                //    // Neu so luong nhay' trong 1 ngay > 20 => Uu Tien 1

                //    if (mucUT == 1)
                //    {
                //        UT1.Add(itemUT);
                //        var queryCheck = "Select CASE_ALARM, TRAM,TINH, NGAY, SO_LAN_NHAY, MUC_UU_TIEN, DA_XU_LY from LICHSUCANHBAO where DA_XU_LY = 0 and MUC_UU_TIEN = 1 and TRAM = '" + itemUT.Site + "' and NGAY < '" + DateTime.Now.AddDays(-dayBefore + 1).ToString("MM/dd/yyyy") + "' order by CASE_ALARM";
                //        var dtCheck = new DataTable();
                //        dtCheck = db.SelectQuery(queryCheck);
                //        itemUT.SoNgayDat = dtCheck.Rows.Count;
                //    }
                //    // Neu so nhay' 1 ngay > 10 => Uu Tien 2
                //    else if (mucUT == 2)
                //    {
                //        UT2.Add(itemUT);
                //        var queryCheck = "Select CASE_ALARM, TRAM,TINH, NGAY, SO_LAN_NHAY, MUC_UU_TIEN, DA_XU_LY from LICHSUCANHBAO where DA_XU_LY = 0 and MUC_UU_TIEN = 2 and TRAM = '" + itemUT.Site + "' and NGAY < '" + DateTime.Now.AddDays(-dayBefore + 1).ToString("MM/dd/yyyy") + "' order by CASE_ALARM";
                //        var dtCheck = new DataTable();
                //        dtCheck = db.SelectQuery(queryCheck);
                //        itemUT.SoNgayDat = dtCheck.Rows.Count;
                //    }
                //}
            }

            var queryTimNhayTheoNgay = "Select TRAM, SO_LAN_NHAY from LICHSUCANHBAO where DA_XU_LY = 0 and NGAY >= '" + DateTime.Now.AddDays(-dayBefore).ToString("MM/dd/yyyy") + "' and NGAY < '" + DateTime.Now.AddDays(-dayBefore + 1).ToString("MM/dd/yyyy") + "'order by TRAM";
            var dtTramNhay = new DataTable();
            dtTramNhay = dbSyncLost.SelectQuery(queryTimNhayTheoNgay);

            foreach (DataRow dr in dtTramNhay.Rows)
            {
                var soNhayNgay = Convert.ToInt32(dr["SO_LAN_NHAY"].ToString());
                var site = dr["TRAM"].ToString();
                for (var i = 0; i < UT1.Count; i++)
                {
                    if (UT1[i].Site == site)
                    {
                        UT1[i].SoLuong = soNhayNgay;
                    }
                }
                for (var i = 0; i < UT2.Count; i++)
                {
                    if (UT2[i].Site == site)
                    {
                        UT2[i].SoLuong = soNhayNgay;
                    }
                }
                for (var i = 0; i < UT3.Count; i++)
                {
                    if (UT3[i].Site == site)
                    {
                        UT3[i].SoLuong = soNhayNgay;
                    }
                }
            }
            #endregion

            #region GET DATA CANH BAO TUAN TONG HOP
            var listCanhBaoTuanTongHop = new List<CanhBaoTongHop>();
            listCanhBaoTuanTongHop = listCanhBaoNgayTongHop;

            //Data ngay Tong hop
            //var queryCanhBaoTuan = "Select TINH,TRAM, CASE_ALARM, MUC_UU_TIEN,  COUNT(NGAY) as SO_LUONG from LICHSUCANHBAO where DA_XU_LY = 0 and NGAY < '" + DateTime.Now.AddDays(-dayBefore + 1).ToString("MM/dd/yyyy") + "' GROUP BY MUC_UU_TIEN, TINH,CASE_ALARM, TRAM order by TINH, MUC_UU_TIEN";
            //var dtCanhBaoTuan = new DataTable();
            //dtCanhBaoTuan = db.SelectQuery(queryCanhBaoTuan);

            //var cbTuan = new CanhBaoTongHop();
            //var countRowTuan = 0;
            //foreach (DataRow dr in dtCanhBaoTuan.Rows)
            //{
            //    countRowTuan++;
            //    if (cbTuan.Tinh != dr["TINH"].ToString())
            //    {
            //        if (cbTuan.Tinh != null)
            //        {
            //            listCanhBaoTuanTongHop.Add(cbTuan);
            //            cbTuan = new CanhBaoTongHop();
            //        }

            //        cbTuan.Tinh = dr["TINH"].ToString();
            //        var mucUuuTien = Convert.ToInt32(dr["MUC_UU_TIEN"]);
            //        var soLuong = Convert.ToInt32(dr["SO_LUONG"]);
            //        if (mucUuuTien == 1)
            //        {
            //            cbTuan.UT1 += soLuong;
            //        }
            //        else if (mucUuuTien == 2)
            //        {
            //            cbTuan.UT2 += soLuong;
            //        }

            //        if (countRowTuan == dtCanhBaoTuan.Rows.Count)
            //        {
            //            listCanhBaoTuanTongHop.Add(cbTuan);
            //        }
            //    }
            //    else
            //    {
            //        var mucUuuTien = Convert.ToInt32(dr["MUC_UU_TIEN"]);
            //        var soLuong = Convert.ToInt32(dr["SO_LUONG"]);
            //        if (mucUuuTien == 1)
            //        {
            //            cbTuan.UT1 += soLuong;
            //        }
            //        else if (mucUuuTien == 2)
            //        {
            //            cbTuan.UT2 += soLuong;
            //        }

            //        if (countRowTuan == dtCanhBaoTuan.Rows.Count)
            //        {
            //            listCanhBaoTuanTongHop.Add(cbTuan);
            //        }
            //    }
            //}

            #endregion

            #region GET DATA CANH BAO THANG TONG HOP

            var listCanhBaoThangTongHop = new List<CanhBaoTongHop>();
            listCanhBaoThangTongHop = listCanhBaoNgayTongHop;

            //Data Thang Tong hop
            //var date = DateTime.Now.AddDays(-dayBefore);
            //var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);

            //var queryCanhBaoThang = "Select TINH,TRAM, CASE_ALARM, MUC_UU_TIEN,  COUNT(NGAY) as SO_LUONG from LICHSUCANHBAO where DA_XU_LY = 0 and NGAY < '" + DateTime.Now.AddDays(-dayBefore + 1).ToString("MM/dd/yyyy") + "' GROUP BY MUC_UU_TIEN, TINH,CASE_ALARM, TRAM order by TINH, MUC_UU_TIEN";
            //var dtCanhBaoThang = new DataTable();
            //dtCanhBaoThang = db.SelectQuery(queryCanhBaoThang);

            //var cbThang = new CanhBaoTongHop();
            //var countRowThang = 0;
            //foreach (DataRow dr in dtCanhBaoThang.Rows)
            //{
            //    countRowThang++;
            //    if (cbThang.Tinh != dr["TINH"].ToString())
            //    {
            //        if (cbThang.Tinh != null)
            //        {
            //            listCanhBaoThangTongHop.Add(cbThang);
            //            cbThang = new CanhBaoTongHop();
            //        }

            //        cbThang.Tinh = dr["TINH"].ToString();
            //        var mucUuuTien = Convert.ToInt32(dr["MUC_UU_TIEN"]);
            //        var soLuong = Convert.ToInt32(dr["SO_LUONG"]);
            //        if (mucUuuTien == 1)
            //        {
            //            cbThang.UT1 += soLuong;
            //        }
            //        else if (mucUuuTien == 2)
            //        {
            //            cbThang.UT2 += soLuong;
            //        }

            //        if (countRowThang == dtCanhBaoThang.Rows.Count)
            //        {
            //            listCanhBaoThangTongHop.Add(cbThang);
            //        }
            //    }
            //    else
            //    {
            //        var mucUuuTien = Convert.ToInt32(dr["MUC_UU_TIEN"]);
            //        var soLuong = Convert.ToInt32(dr["SO_LUONG"]);
            //        if (mucUuuTien == 1)
            //        {
            //            cbThang.UT1 += soLuong;
            //        }
            //        else if (mucUuuTien == 2)
            //        {
            //            cbThang.UT2 += soLuong;
            //        }

            //        if (countRowThang == dtCanhBaoThang.Rows.Count)
            //        {
            //            listCanhBaoThangTongHop.Add(cbThang);
            //        }
            //    }
            //}

            #endregion

            #region GET DATA NGAY XU LY OK
            Get_Data_Xu_Ly_Ok_Ngay();//Phuc vu cho bang XU LY OK NGAY

            listXuLyOKNgayTongHopSyncLost = Get_Data_Xu_Ly_Ok_Ngay_Tong_Hop();
            #endregion

            #region GET DATA TUAN XU LY OK

            listXuLyOKTuanTongHopSyncLost = Get_Data_Xu_Ly_Ok_Tuan();

            #endregion

            #region GET DATA THANG XU LY OK

            listXuLyOKThangTongHopSyncLost = Get_Data_Xu_Ly_Ok_Thang();

            #endregion

            #region TAO CONTENT EMAIL TABLE TONG HOP

            var tongHopDai = new CanhBaoTongHop();
            var listTinhFull = new List<string>();
            listTinhFull.Add("Quang Ngai");
            listTinhFull.Add("Khanh Hoa");
            listTinhFull.Add("Da Nang");
            listTinhFull.Add("Dac Nong");
            listTinhFull.Add("Quang Nam");
            listTinhFull.Add("TT-Hue");
            listTinhFull.Add("Binh Dinh");
            listTinhFull.Add("Kon Tum");
            listTinhFull.Add("Phu Yen");
            listTinhFull.Add("Dak Lak");
            listTinhFull.Add("Quang Tri");
            listTinhFull.Add("Gia Lai");

            listTinhSyncLost = listCanhBaoNgayTongHop.Select(_ => _.Tinh).ToList();
            for (var j = 0; j < listTinhFull.Count; j++)
            {
                if (!listTinhSyncLost.Any(x => x == listTinhFull[j]))
                {
                    cb = new CanhBaoTongHop();
                    cb.Tinh = listTinhFull[j];

                    if (cb.Tinh == "Quang Tri" || cb.Tinh == "TT-Hue" || cb.Tinh == "Da Nang" || cb.Tinh == "Quang Nam")
                    {
                        cb.DVT = "ĐVTĐN";
                    }
                    else if (cb.Tinh == "Quang Ngai" || cb.Tinh == "Phu Yen" || cb.Tinh == "Binh Dinh" || cb.Tinh == "Khanh Hoa")
                    {
                        cb.DVT = "ĐVTBĐ";
                    }
                    else if (cb.Tinh == "Gia Lai" || cb.Tinh == "Kon Tum" || cb.Tinh == "Dak Lak" || cb.Tinh == "Dac Nong")
                    {
                        cb.DVT = "ĐVTĐL";
                    }

                    listCanhBaoNgayTongHop.Add(cb);
                }
            }

            var listCanhBaoNgayTongHopSort = listCanhBaoNgayTongHop.OrderBy(_ => _.DVT).ToList();


            for (var j = 0; j < listCanhBaoNgayTongHopSort.Count(); j++)
            {

                var xuLyOkNgayByTinh = listXuLyOKNgayTongHopSyncLost.FirstOrDefault(_ => _.Tinh == listCanhBaoNgayTongHopSort[j].Tinh);
                if (xuLyOkNgayByTinh == null)
                {
                    xuLyOkNgayByTinh = new XuLyOkTongHop();
                }

                var xuLyOkTuanByTinh = listXuLyOKTuanTongHopSyncLost.FirstOrDefault(_ => _.Tinh == listCanhBaoNgayTongHopSort[j].Tinh);
                if (xuLyOkTuanByTinh == null)
                {
                    xuLyOkTuanByTinh = new XuLyOkTongHop();
                }
                var xuLyOkThangByTinh = listXuLyOKThangTongHopSyncLost.FirstOrDefault(_ => _.Tinh == listCanhBaoNgayTongHopSort[j].Tinh);
                if (xuLyOkThangByTinh == null)
                {
                    xuLyOkThangByTinh = new XuLyOkTongHop();
                }

                var canhBaoTuanByTinh = listCanhBaoTuanTongHop.FirstOrDefault(_ => _.Tinh == listCanhBaoNgayTongHopSort[j].Tinh);
                if (canhBaoTuanByTinh == null)
                {
                    canhBaoTuanByTinh = new CanhBaoTongHop();
                }

                var canhBaoThangByTinh = listCanhBaoThangTongHop.FirstOrDefault(_ => _.Tinh == listCanhBaoNgayTongHopSort[j].Tinh);
                if (canhBaoThangByTinh == null)
                {
                    canhBaoThangByTinh = new CanhBaoTongHop();
                }

                if (tongHopDai.DVT != listCanhBaoNgayTongHopSort[j].DVT)
                {
                    if (tongHopDai.DVT != null)
                    {
                        rowsOfTableSyncLost2 += "<tr>" +

                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #98b2e4; white-space: nowrap;'>" + tongHopDai.DVT + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #bdbc80; white-space: nowrap; font-style: italic'>Tổng</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7' >" + (tongHopDai.UT1) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7' >" + (tongHopDai.UT2) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7' >" + (tongHopDai.UT3) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7' >" + (tongHopDai.UT1 + tongHopDai.UT2 + tongHopDai.UT3) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7'>" + (tongHopDai.SoLuongTuan) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7'>" + (tongHopDai.ChuaXuLyTuan) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7'>" + (tongHopDai.SoLuongTuan == 0 ? 0 : 100 * (tongHopDai.ChuaXuLyTuan) / (tongHopDai.SoLuongTuan)) + "%</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7'>" + (tongHopDai.SoLuongThang) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7'>" + (tongHopDai.ChuaXuLyThang) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7'>" + (tongHopDai.SoLuongThang == 0 ? 0 : 100 * (tongHopDai.ChuaXuLyThang) / (tongHopDai.SoLuongThang)) + "%</td>" +

                                     "</tr>";
                        tongHopDai = new CanhBaoTongHop();
                    }
                    tongHopDai.DVT = listCanhBaoNgayTongHopSort[j].DVT;
                    tongHopDai.Tinh = " ";
                    tongHopDai.UT1 += listCanhBaoNgayTongHopSort[j].UT1;
                    tongHopDai.UT2 += listCanhBaoNgayTongHopSort[j].UT2;
                    tongHopDai.UT3 += listCanhBaoNgayTongHopSort[j].UT3;
                    tongHopDai.SoLuongTuan += canhBaoTuanByTinh.UT1 + canhBaoTuanByTinh.UT2 + canhBaoTuanByTinh.UT3 + xuLyOkTuanByTinh.SoLuong;
                    tongHopDai.SoLuongThang += canhBaoThangByTinh.UT1 + canhBaoThangByTinh.UT2 + canhBaoThangByTinh.UT3 + xuLyOkThangByTinh.SoLuong;
                    tongHopDai.ChuaXuLyTuan += canhBaoTuanByTinh.UT1 + canhBaoTuanByTinh.UT2 + canhBaoTuanByTinh.UT3;
                    tongHopDai.ChuaXuLyThang += canhBaoThangByTinh.UT1 + canhBaoThangByTinh.UT2 + canhBaoThangByTinh.UT3;
                }
                else
                {
                    tongHopDai.DVT = listCanhBaoNgayTongHopSort[j].DVT;
                    tongHopDai.Tinh = "";
                    tongHopDai.UT1 += listCanhBaoNgayTongHopSort[j].UT1;
                    tongHopDai.UT2 += listCanhBaoNgayTongHopSort[j].UT2;
                    tongHopDai.UT3 += listCanhBaoNgayTongHopSort[j].UT3;
                    tongHopDai.SoLuongTuan += canhBaoTuanByTinh.UT1 + canhBaoTuanByTinh.UT2 + canhBaoTuanByTinh.UT3 + xuLyOkTuanByTinh.SoLuong;
                    tongHopDai.SoLuongThang += canhBaoThangByTinh.UT1 + canhBaoThangByTinh.UT2 + canhBaoThangByTinh.UT3 + xuLyOkThangByTinh.SoLuong;
                    tongHopDai.ChuaXuLyTuan += canhBaoTuanByTinh.UT1 + canhBaoTuanByTinh.UT2 + canhBaoTuanByTinh.UT3;
                    tongHopDai.ChuaXuLyThang += canhBaoThangByTinh.UT1 + canhBaoThangByTinh.UT2 + canhBaoThangByTinh.UT3;
                }
                rowsOfTableSyncLost2 += "<tr>" +

                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #98b2e4; white-space: nowrap;'>" + listCanhBaoNgayTongHopSort[j].DVT + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #bdbc80; white-space: nowrap;'>" + listCanhBaoNgayTongHopSort[j].Tinh + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #FFF' >" + (listCanhBaoNgayTongHopSort[j].UT1) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #FFF' >" + (listCanhBaoNgayTongHopSort[j].UT2) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #FFF' >" + (listCanhBaoNgayTongHopSort[j].UT3) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #FFF' >" + (listCanhBaoNgayTongHopSort[j].UT1 + listCanhBaoNgayTongHopSort[j].UT2 + listCanhBaoNgayTongHopSort[j].UT3) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;'>" + (canhBaoTuanByTinh.UT1 + canhBaoTuanByTinh.UT2 + canhBaoTuanByTinh.UT3 + xuLyOkTuanByTinh.SoLuong) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;'>" + (canhBaoTuanByTinh.UT1 + canhBaoTuanByTinh.UT2 + canhBaoTuanByTinh.UT3) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;'>" + (canhBaoTuanByTinh.UT1 + canhBaoTuanByTinh.UT2 + canhBaoTuanByTinh.UT3 + xuLyOkTuanByTinh.SoLuong == 0 ? 0 : 100 * (canhBaoTuanByTinh.UT1 + canhBaoTuanByTinh.UT2 + canhBaoTuanByTinh.UT3) / (canhBaoTuanByTinh.UT1 + canhBaoTuanByTinh.UT2 + canhBaoTuanByTinh.UT3 + xuLyOkTuanByTinh.SoLuong)) + "%</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;'>" + (canhBaoThangByTinh.UT1 + canhBaoThangByTinh.UT2 + canhBaoThangByTinh.UT3 + xuLyOkThangByTinh.SoLuong) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;'>" + (canhBaoThangByTinh.UT1 + canhBaoThangByTinh.UT2 + canhBaoThangByTinh.UT3) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;'>" + (canhBaoThangByTinh.UT1 + canhBaoThangByTinh.UT2 + canhBaoThangByTinh.UT3 + xuLyOkThangByTinh.SoLuong == 0 ? 0 : 100 * (canhBaoThangByTinh.UT1 + canhBaoThangByTinh.UT2 + canhBaoThangByTinh.UT3) / (canhBaoThangByTinh.UT1 + canhBaoThangByTinh.UT2 + canhBaoThangByTinh.UT3 + xuLyOkThangByTinh.SoLuong)) + "%</td>" +

                                     "</tr>";

                var tongCaseOpen = (listCanhBaoNgayTongHopSort[j].UT1 + listCanhBaoNgayTongHopSort[j].UT2 + listCanhBaoNgayTongHopSort[j].UT3);
                string queryInsertSoLieuCaseOpen = "INSERT INTO SOLIEUCASEALARM(TINH,NGAY,SO_CASE_OPEN) VALUES(@TINH, @NGAY, @SO_CASE_OPEN)";
                var command = new SqlCommand();

                command.Parameters.Add("TINH", SqlDbType.NVarChar).Value = listCanhBaoNgayTongHopSort[j].Tinh;
                command.Parameters.Add("NGAY", SqlDbType.DateTime).Value = DateTime.Now.AddDays(-dayBefore);
                command.Parameters.Add("SO_CASE_OPEN", SqlDbType.Int).Value = tongCaseOpen;

                var queryCheckExist = "Select * from SOLIEUCASEALARM where TINH='" + listCanhBaoNgayTongHopSort[j].Tinh + "' and NGAY='" + DateTime.Now.AddDays(-dayBefore).ToString("MM-dd-yyyy") + "'";

                var dtCheck = dbSyncLost.SelectQuery(queryCheckExist);

                if (dtCheck.Rows.Count == 0)
                {
                    dbSyncLost.ExecuteQuery(command, queryInsertSoLieuCaseOpen);
                }


                if (j == listCanhBaoNgayTongHopSort.Count() - 1)
                {
                    rowsOfTableSyncLost2 += "<tr>" +

                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #98b2e4; white-space: nowrap;'>" + tongHopDai.DVT + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #bdbc80; white-space: nowrap; font-style: italic'>Tổng</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7' >" + (tongHopDai.UT1) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7' >" + (tongHopDai.UT2) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7' >" + (tongHopDai.UT3) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center;  background: #f1c4e7' >" + (tongHopDai.UT1 + tongHopDai.UT2 + tongHopDai.UT3) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #f1c4e7'>" + (tongHopDai.SoLuongTuan) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #f1c4e7'>" + (tongHopDai.ChuaXuLyTuan) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #f1c4e7'>" + (tongHopDai.SoLuongTuan == 0 ? 0 : 100 * (tongHopDai.ChuaXuLyTuan) / (tongHopDai.SoLuongTuan)) + "%</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #f1c4e7'>" + (tongHopDai.SoLuongThang) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #f1c4e7'>" + (tongHopDai.ChuaXuLyThang) + "</td>" +
                                        "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #f1c4e7'>" + (tongHopDai.SoLuongThang == 0 ? 0 : 100 * (tongHopDai.ChuaXuLyThang) / (tongHopDai.SoLuongThang)) + "%</td>" +

                                     "</tr>";
                }
            }
            #endregion

            #region TAO CONTENT EMAIL TABLE CANH BAO NGAY

            var listSite = new List<string>();

            var UT1SortTemp = UT1.OrderByDescending(_ => _.Tinh).ThenByDescending(_ => _.SoNgayDat).ToList();
            var UT2SortTemp = UT2.OrderByDescending(_ => _.Tinh).ThenByDescending(_ => _.SoNgayDat).ToList();
            var UT3SortTemp = UT3.OrderByDescending(_ => _.Tinh).ThenByDescending(_ => _.SoNgayDat).ToList();

            var UT1Sort = new List<SoCanhBaoUuTienTheoSite>();
            var UT2Sort = new List<SoCanhBaoUuTienTheoSite>();
            var UT3Sort = new List<SoCanhBaoUuTienTheoSite>();

            var dem = 0;
            for (var i = 0; i < UT1SortTemp.Count; i++)
            {
                if (i == 0)
                {
                    UT1Sort.Add(UT1SortTemp[i]);
                    dem++;
                }
                else if (UT1SortTemp[i].Tinh != UT1SortTemp[i - 1].Tinh)
                {
                    dem = 0;
                    UT1Sort.Add(UT1SortTemp[i]);
                    dem++;
                }
                else
                {
                    if (dem < 5)
                    {
                        UT1Sort.Add(UT1SortTemp[i]);
                        dem++;
                    }
                }
            }
            dem = 0;
            for (var i = 0; i < UT2SortTemp.Count; i++)
            {
                if (i == 0)
                {
                    UT2Sort.Add(UT2SortTemp[i]);
                    dem++;
                }
                else if (UT2SortTemp[i].Tinh != UT2SortTemp[i - 1].Tinh)
                {
                    dem = 0;
                    UT2Sort.Add(UT2SortTemp[i]);
                    dem++;
                }
                else
                {
                    if (dem < 5)
                    {
                        UT2Sort.Add(UT2SortTemp[i]);
                        dem++;
                    }
                }
            }
            dem = 0;
            for (var i = 0; i < UT3SortTemp.Count; i++)
            {
                if (i == 0)
                {
                    UT3Sort.Add(UT3SortTemp[i]);
                    dem++;
                }
                else if (UT3SortTemp[i].Tinh != UT3SortTemp[i - 1].Tinh)
                {
                    dem = 0;
                    UT3Sort.Add(UT3SortTemp[i]);
                    dem++;
                }
                else
                {
                    if (dem < 5)
                    {
                        UT3Sort.Add(UT3SortTemp[i]);
                        dem++;
                    }
                }
            }

            var lengthMax = Math.Max(Math.Max(UT1Sort.Count, UT2Sort.Count), UT3Sort.Count);

            for (var i = 0; i < lengthMax; i++)
            {
                var ut1Site = "";
                var ut1SoLuongNhay = "";
                var ut1SoNgayLienTuc = "";
                var ut1SoLuongNhayTB = "";

                var ut2Site = "";
                var ut2SoLuongNhay = "";
                var ut2SoNgayLienTuc = "";
                var ut2SoLuongNhayTB = "";

                var ut3Site = "";
                var ut3SoLuongNhay = "";
                var ut3SoNgayLienTuc = "";
                var ut3SoLuongNhayTB = "";

                if (i <= UT1Sort.Count - 1)
                {
                    ut1Site = UT1Sort[i].Site;
                    ut1SoLuongNhay = UT1Sort[i].SoLuong.ToString();
                    ut1SoNgayLienTuc = UT1Sort[i].SoNgayDat.ToString();
                    ut1SoLuongNhayTB = ut1SoNgayLienTuc == "0" ? "0" : (UT1Sort[i].SoLuongCongDon / UT1Sort[i].SoNgayDat).ToString();
                    var siteId = ut1Site.Substring(0, 4);
                    if (!listSite.Any(t => t == siteId))
                    {
                        listSite.Add(siteId);
                    }

                }
                else
                {
                    ut1Site = "";
                    ut1SoLuongNhay = "";
                    ut1SoNgayLienTuc = "";
                    ut1SoLuongNhayTB = "";
                }

                if (i <= UT2Sort.Count - 1)
                {
                    ut2Site = UT2Sort[i].Site;
                    ut2SoLuongNhay = UT2Sort[i].SoLuong.ToString();
                    ut2SoNgayLienTuc = UT2Sort[i].SoNgayDat.ToString();
                    ut2SoLuongNhayTB = ut2SoNgayLienTuc == "0" ? "0" : (UT2Sort[i].SoLuongCongDon / UT2Sort[i].SoNgayDat).ToString();
                    var siteId = ut2Site.Substring(0, 4);
                    if (!listSite.Any(t => t == siteId))
                    {
                        listSite.Add(siteId);
                    }
                }
                else
                {
                    ut2Site = "";
                    ut2SoLuongNhay = "";
                    ut2SoNgayLienTuc = "";
                    ut2SoLuongNhayTB = "";
                }

                if (i <= UT3Sort.Count - 1)
                {
                    ut3Site = UT3Sort[i].Site;
                    ut3SoLuongNhay = UT3Sort[i].SoLuong.ToString();
                    ut3SoNgayLienTuc = UT3Sort[i].SoNgayDat.ToString();
                    ut3SoLuongNhayTB = ut3SoNgayLienTuc == "0" ? "0" : (UT3Sort[i].SoLuongCongDon / UT3Sort[i].SoNgayDat).ToString();
                    var siteId = ut3Site.Substring(0, 4);
                    if (!listSite.Any(t => t == siteId))
                    {
                        listSite.Add(siteId);
                    }
                }
                else
                {
                    ut3Site = "";
                    ut3SoLuongNhay = "";
                    ut3SoNgayLienTuc = "";
                    ut3SoLuongNhayTB = "";
                }

                rowsOfTableSyncLost += "<tr>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #dad4d9' >" + ut1Site + "</td>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #FFF' >" + ut1SoNgayLienTuc + "</td>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #FFF' >" + ut1SoLuongNhayTB + "</td>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #FFF' >" + ut1SoLuongNhay + "</td>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #dad4d9' >" + ut2Site + "</td>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #FFF' >" + ut2SoNgayLienTuc + "</td>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #FFF' >" + ut2SoLuongNhayTB + "</td>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #FFF' >" + ut2SoLuongNhay + "</td>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #dad4d9' >" + ut3Site + "</td>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #FFF' >" + ut3SoNgayLienTuc + "</td>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #FFF' >" + ut3SoLuongNhayTB + "</td>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #FFF' >" + ut3SoLuongNhay + "</td>" +
                           "</tr>";
            }

            #region MAKE ATTACHMENT FILE

            string fileName = "Attachment_Template_File.xlsx";
            string fileNameDest = "Attachment_File_CanhBaoNgay_" + DateTime.Now.AddDays(-dayBefore).ToString("yyyyMMddHHmmss") + ".xlsx";

            string sourcePath = @"D:\SynLost\Default";
            string targetPath = @"D:\SynLost\Result";

            fileAttachmentPathSyncLost = @"D:\SynLost\Result\" + "Attachment_File_CanhBaoNgay_" + DateTime.Now.AddDays(-dayBefore).ToString("yyyyMMddHHmmss") + ".xlsx";
            var connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileAttachmentPathSyncLost + ";Extended Properties=\'Excel 12.0;HDR=Yes;TypeGuessRows=0;ImportMixedTypes=Text\'";


            // Use Path class to manipulate file and directory paths.
            string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
            string destFile = System.IO.Path.Combine(targetPath, fileNameDest);

            // To copy a folder's contents to a new location:
            // Create a new target folder, if necessary.
            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }

            // To copy a file to another location and 
            // overwrite the destination file if it already exists.
            File.Copy(sourceFile, destFile, true);

            using (OleDbConnection conn = new OleDbConnection(connectionString))
            {
                var command = new System.Data.OleDb.OleDbCommand();
                string sql = null;
                conn.Open();

                command.Connection = conn;

                var lengthMax2 = Math.Max(Math.Max(UT1SortTemp.Count, UT2SortTemp.Count), UT3SortTemp.Count);

                for (var i = 0; i < lengthMax2; i++)
                {
                    var ut1Site = "";
                    var ut1SoLuongNhay = "";
                    var ut1SoNgayLienTuc = "";
                    var ut1SoLuongNhayTB = "";

                    var ut2Site = "";
                    var ut2SoLuongNhay = "";
                    var ut2SoNgayLienTuc = "";
                    var ut2SoLuongNhayTB = "";

                    var ut3Site = "";
                    var ut3SoLuongNhay = "";
                    var ut3SoNgayLienTuc = "";
                    var ut3SoLuongNhayTB = "";

                    if (i <= UT1SortTemp.Count - 1)
                    {
                        ut1Site = UT1SortTemp[i].Site;
                        ut1SoLuongNhay = UT1SortTemp[i].SoLuong.ToString();
                        ut1SoNgayLienTuc = UT1SortTemp[i].SoNgayDat.ToString();
                        ut1SoLuongNhayTB = ut1SoNgayLienTuc == "0" ? "0" : (UT1SortTemp[i].SoLuongCongDon / UT1SortTemp[i].SoNgayDat).ToString();
                        var siteId = ut1Site.Substring(0, 4);
                        if (!listSite.Any(t => t == siteId))
                        {
                            listSite.Add(siteId);
                        }

                    }
                    else
                    {
                        ut1Site = "";
                        ut1SoLuongNhay = "";
                        ut1SoNgayLienTuc = "";
                        ut1SoLuongNhayTB = "";
                    }

                    if (i <= UT2SortTemp.Count - 1)
                    {
                        ut2Site = UT2SortTemp[i].Site;
                        ut2SoLuongNhay = UT2SortTemp[i].SoLuong.ToString();
                        ut2SoNgayLienTuc = UT2SortTemp[i].SoNgayDat.ToString();
                        ut2SoLuongNhayTB = ut2SoNgayLienTuc == "0" ? "0" : (UT2SortTemp[i].SoLuongCongDon / UT2SortTemp[i].SoNgayDat).ToString();
                        var siteId = ut2Site.Substring(0, 4);
                        if (!listSite.Any(t => t == siteId))
                        {
                            listSite.Add(siteId);
                        }
                    }
                    else
                    {
                        ut2Site = "";
                        ut2SoLuongNhay = "";
                        ut2SoNgayLienTuc = "";
                        ut2SoLuongNhayTB = "";
                    }

                    if (i <= UT3SortTemp.Count - 1)
                    {
                        ut3Site = UT3SortTemp[i].Site;
                        ut3SoLuongNhay = UT3SortTemp[i].SoLuong.ToString();
                        ut3SoNgayLienTuc = UT3SortTemp[i].SoNgayDat.ToString();
                        ut3SoLuongNhayTB = ut3SoNgayLienTuc == "0" ? "0" : (UT3SortTemp[i].SoLuongCongDon / UT3SortTemp[i].SoNgayDat).ToString();
                        var siteId = ut3Site.Substring(0, 4);
                        if (!listSite.Any(t => t == siteId))
                        {
                            listSite.Add(siteId);
                        }
                    }
                    else
                    {
                        ut3Site = "";
                        ut3SoLuongNhay = "";
                        ut3SoNgayLienTuc = "";
                        ut3SoLuongNhayTB = "";
                    }

                    sql = "Insert into [Sheet1$] values('" + ut1Site + "','" + ut1SoNgayLienTuc + "','" + ut1SoLuongNhayTB + "','" + ut1SoLuongNhay + "','" + ut2Site + "','" + ut2SoNgayLienTuc + "','" + ut2SoLuongNhayTB + "','" + ut2SoLuongNhay + "','" + ut3Site + "','" + ut3SoNgayLienTuc + "','" + ut3SoLuongNhayTB + "','" + ut3SoLuongNhay + "')";
                    command.CommandText = sql;
                    command.ExecuteNonQuery();
                }

            }
            #endregion

            #endregion

            #region TAO CONTENT EMAIL TABLE XU_LY_OK NGAY
            lengthMax = XuLyOkNgaySyncLost.Count;

            for (var i = 0; i < lengthMax; i++)
            {
                var site = "";
                var ngayBatDau = "";
                var ngayXuLy = "";
                var tinh = "";
                var dvt = "";
                var soNgayKeoDai = "";
                if (i <= XuLyOkNgaySyncLost.Count - 1)
                {
                    site = XuLyOkNgaySyncLost[i].Site;
                    ngayBatDau = XuLyOkNgaySyncLost[i].Ngay;
                    ngayXuLy = XuLyOkNgaySyncLost[i].NgayXuLy;
                    tinh = XuLyOkNgaySyncLost[i].Tinh;
                    dvt = XuLyOkNgaySyncLost[i].DVT;
                    soNgayKeoDai = XuLyOkNgaySyncLost[i].SoNgayKeoDai.ToString();
                }
                else
                {
                    site = "";
                    ngayBatDau = "";
                    ngayXuLy = "";
                    tinh = "";
                    dvt = "";
                    soNgayKeoDai = "";
                }

                rowsOfTableSyncLost3 += "<tr>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #e06868' >" + dvt + "</td>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #e06868' >" + tinh + "</td>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #e06868' >" + site + "</td>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #e06868' >" + ngayBatDau + "</td>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #e06868' >" + ngayXuLy + "</td>" +
                               "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; background: #e06868' >" + soNgayKeoDai + "</td>" +
                           "</tr>";
            }

            #endregion

            SendEmail();
        }

        public List<string> GetListMail()
        {
            var getMailQuery = "SELECT * FROM LISTEMAIL";
            var dbMail = dbSyncLost.SelectQuery(getMailQuery);

            var listMail = new List<string>();

            for (var i = 0; i < dbMail.Rows.Count; i++)
            {
                var mail = dbMail.Rows[i]["EMAIL"].ToString();

                listMail.Add(mail);
            }

            return listMail;
        }
        public List<string> GetListMailByTableName(string table)
        {
            var getMailQuery = "SELECT * FROM " + table;
            var dbMail = dbSyncLost.SelectQuery(getMailQuery);

            var listMail = new List<string>();

            for (var i = 0; i < dbMail.Rows.Count; i++)
            {
                var mail = dbMail.Rows[i]["EMAIL"].ToString();

                listMail.Add(mail);
            }

            return listMail;
        }

        private void btnTestReport_Click(object sender, EventArgs e)
        {
            if (txtDayBefore.Text == "") dayBefore = 1;
            startDayOfWeek = Get_Start_DayOfWeek();
            TestReport();

        }
        private void button1_Click(object sender, EventArgs e)
        {
            MayPhatDienAlarm();
        }
        public void MayPhatDienAlarm()
        {
            var comd = new OracleCommand();
            comd.Connection = con;

            var comdLog = new OracleCommand();
            comdLog.Connection = con;


            comd.CommandText = "select SITE, to_char(SDATE,'dd/MM/yyyy HH24:mi:ss') as SDATE, to_char(EDATE,'dd/MM/yyyy HH24:mi:ss') as EDATE," +
                                "(case " +
                                " when EDATE is null then ROUND((trunc(SYSDATE)-SDATE)*1440,1)" +
                                " when trunc(EDATE) = trunc(SDATE)then ROUND((EDATE-SDATE)*1440,1)" +
                                " when trunc(EDATE) > trunc(SDATE) and trunc(sysdate-1) = trunc(sdate)then ROUND((trunc(sysdate)-SDATE)*1440,1)" +
                                " when trunc(EDATE) > trunc(SDATE) and trunc(sysdate-1) = trunc(edate)then ROUND((EDATE-trunc(sysdate-1))*1440,1)" +
                                " end) as DURATION" +
                                " from MLMT_MP_DIEN" +
                                " where (SDATE BETWEEN TRUNC(SYSDATE - 1) AND TRUNC(SYSDATE) - 1/86400) or trunc(EDATE)=trunc(sysdate - 1)" +
                                " order by SITE";
            var da = new OracleDataAdapter(comd);
            var dtMPD = new DataTable();
            da.Fill(dtMPD);

            #region MAKE ATTACHMENT FILE

            string fileName = "Attachment_MPD.xlsx";
            string fileNameDest = "Attachment_MPD_ThongKeNgay_" + DateTime.Now.AddDays(-dayBefore).ToString("yyyyMMddHHmmss") + ".xlsx";

            string sourcePath = @"D:\SynLost\Default";
            string targetPath = @"D:\SynLost\Result";

            var fileAttachmentPathMPD = @"D:\SynLost\Result\" + "Attachment_MPD_ThongKeNgay_" + DateTime.Now.AddDays(-dayBefore).ToString("yyyyMMddHHmmss") + ".xlsx";
            var connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileAttachmentPathMPD + ";Extended Properties=\'Excel 12.0;HDR=Yes;TypeGuessRows=0;ImportMixedTypes=Text\'";


            // Use Path class to manipulate file and directory paths.
            string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
            string destFile = System.IO.Path.Combine(targetPath, fileNameDest);

            // To copy a folder's contents to a new location:
            // Create a new target folder, if necessary.
            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }

            // To copy a file to another location and 
            // overwrite the destination file if it already exists.
            File.Copy(sourceFile, destFile, true);

            using (OleDbConnection conn = new OleDbConnection(connectionString))
            {
                var command = new System.Data.OleDb.OleDbCommand();
                string sql = null;
                conn.Open();

                command.Connection = conn;

                for (var i = 0; i < dtMPD.Rows.Count; i++)
                {
                    var site = dtMPD.Rows[i]["SITE"].ToString();
                    var sDate = dtMPD.Rows[i]["SDATE"].ToString();
                    var eDate = dtMPD.Rows[i]["EDATE"].ToString();
                    var duration = dtMPD.Rows[i]["DURATION"].ToString();

                    sql = "Insert into [Sheet1$] values('" + (i + 1) + "','" + site + "','" + sDate + "','" + eDate + "','" + duration + "')";
                    command.CommandText = sql;
                    command.ExecuteNonQuery();
                }

            }
            #endregion

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress("mlmt.cntt@mobifone.vn", "no-reply@mobifone.vn");
            mail.To.Add("mbftunt@gmail.com");
            mail.To.Add("tu.tien.ctv@mobifone.vn");
            if (radioButton_yn_EmailTest_No.Checked == true)
            {
                var listMailAdd = GetListMailByTableName("LISTEMAILMPD");
                for (var i = 0; i < listMailAdd.Count; i++)
                {
                    mail.To.Add(listMailAdd[i]);
                }
            }
            mail.Subject = "Thống kê thời gian chạy máy phát điện " + DateTime.Now.AddDays(-dayBefore).ToString("dd/MM/yyyy");
            mail.IsBodyHtml = true;

            var bodyMail = "<html>" +
                            "<body>" +
                                "<div style='padding-left: 10px;margin: 20px; font-size: 12px;'>" +
                                "<div style='color: #000; font-size: 12px;'>Dear anh/chị,</div><br><br>" +
                                "<div style='color: #000; font-size: 12px;'> Vui lòng kiểm tra số liệu thống kê thời gian chạy máy phát điện ngày " + DateTime.Now.AddDays(-dayBefore).ToString("dd/MM/yyyy") + " trong file đính kèm.</div>" +

                                "<div style='color: #000; font-size: 12px;'><br><br>Best Regard!</div>" +
                                "<div style='color: #000; font-size: 12px;'>...TTMLMT...</div>" +
                                "</div>" +
                             "</body>" +
                       "</html>";

            mail.Body = bodyMail;
            var attachment = new System.Net.Mail.Attachment(fileAttachmentPathMPD);
            mail.Attachments.Add(attachment);

            var tryAgain = 10;
            var failed = false;
            do
            {
                try
                {
                    failed = false;
                    __.SmtpClient().Send(mail);
                }
                catch (Exception ex) // I would avoid catching all exceptions equally, but ymmv
                {
                    failed = true;
                    tryAgain--;
                    var exception = ex.Message.ToString();
                    //Other code for saving exception message to a log.
                    Console.WriteLine("Date: " + DateTime.Now + ". Er: " + ex.Message);
                    //MessageBox.Show("Date: " + DateTime.Now + ". Er: " + ex.Message);
                    lblError.Text = "Date: " + DateTime.Now + ". Er: " + ex.Message;
                }
            } while (failed && tryAgain != 0);

            if (!failed)
            {
                lblError.Text = "";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Get_Report_KPI();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (cbSendEmailDaily.Checked == true)
            {
                var a = 0;
            }
            else
            {
                var b = 0;
            }
        }

        private void btnTestTUNT_Click(object sender, EventArgs e)
        {
            //get du lieu, tao noi dung mail
            __.WriteToRichTextBox(richMessage, "Run btnTestTUNT_Click ");


           //Get_SynLost();

            // send email
            //SendEmail();
            MayPhatDienAlarm();

        }


    }
}
